echo "Setting Git variables for Hostile Harmonics"
export GIT_DIR=/f/Repositories/MajorProjects/HostileHarmonics.git
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export GIT_WORK_TREE=$DIR

echo "Set Git variables"
echo "GIT_DIR: $GIT_DIR"
echo "GIT_WORK_TREE: $GIT_WORK_TREE"
