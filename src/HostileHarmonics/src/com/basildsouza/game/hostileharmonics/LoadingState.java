package com.basildsouza.game.hostileharmonics;

import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import com.basildsouza.game.hostileharmonics.factory.HUDEntityFactory;
import com.basildsouza.game.hostileharmonics.factory.MusicEntityFactory;
import com.basildsouza.game.hostileharmonics.factory.SpriteFactory;
import com.basildsouza.tools.music.piano.engine.Accidental;
import com.basildsouza.tools.music.piano.engine.Clef;
import com.basildsouza.tools.music.piano.engine.Note;
import com.basildsouza.tools.music.piano.engine.NoteLetter;
import com.basildsouza.tools.music.piano.engine.Tone;


public class LoadingState extends BasicGameState{
	public final static int GAME_STATE_ID = GameStateIDContainer.LOADING_STATE;
	
	private SpriteFactory spriteFactory;
	private MusicEntityFactory musicEntityFactory;
	private HUDEntityFactory hudEntityFactory;

	public HUDEntityFactory getHudEntityFactory() {
		return hudEntityFactory;
	}
	
	public MusicEntityFactory getMusicEntityFactory() {
		return musicEntityFactory;
	}
	
	public SpriteFactory getSpriteFactory() {
		return spriteFactory;
	}
	

	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		spriteFactory = new SpriteFactory("images\\");
		musicEntityFactory = new MusicEntityFactory(spriteFactory);
		hudEntityFactory = new HUDEntityFactory(spriteFactory);

		
	}
	
	@Override
	public void enter(GameContainer container, final StateBasedGame game)
			throws SlickException {
	}
	
	public void preCacheSpriteNotes(Clef clef, Tone startTone, Tone endTone) throws SlickException{
		while(startTone.getTonicDistance()<endTone.getTonicDistance()){
			spriteFactory.createQuaterNoteSprite(clef, startTone);
			spriteFactory.createQuaterNoteSprite(clef, startTone.toAccidental(Accidental.Sharp));
			spriteFactory.createQuaterNoteSprite(clef, startTone.toAccidental(Accidental.Flat));
			startTone = startTone.nextNatural();
		}
	}


	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		final UnicodeFont font = new UnicodeFont(new java.awt.Font("Verdana", java.awt.Font.BOLD, 40));
		font.addAsciiGlyphs();
		font.getEffects().add(new ColorEffect(java.awt.Color.red));
		font.loadGlyphs();
		
		Font oldFont = g.getFont();
		g.setFont(font);
		g.drawString("Loading...", 300, 400);
		
		g.setFont(oldFont);
	}

	@Override
	public void update(final GameContainer container, final StateBasedGame game, int delta)
			throws SlickException {
		//TODO: Enventually show loading graphics and do the loading on a different thread or something?
		new Thread(){
			@Override
			public void run() {
				try {
					preCacheSpriteNotes(Clef.Bass, new Tone(new Note(NoteLetter.B), 1), new Tone(new Note(NoteLetter.E), 4));
					preCacheSpriteNotes(Clef.Treble, new Tone(new Note(NoteLetter.G), 3), new Tone(new Note(NoteLetter.C), 6));
					game.enterState(InGamePlayState.GAME_STATE_ID, new FadeOutTransition(), new FadeInTransition());
				} catch (SlickException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.exit(1);
				}
				
			}
		}.run(); // Not starting a new thread
	}

	@Override
	public int getID() {
		return GAME_STATE_ID;
	}
	
}
