package com.basildsouza.game.hostileharmonics.states;

public interface GameStateAware {
	public void init(GameState gameState);
	public void start();
	public void pause();
	public void resume();
	public void end();
	public void reset();
}
