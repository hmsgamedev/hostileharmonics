package com.basildsouza.game.hostileharmonics.states;

public interface GameState {
	public void register(GameStateAware gameStateAware);
	public void unregister(GameStateAware gameStateAware);
	
	public boolean isPaused();
	
	// This prolly should go into a different form of GameState
	public void init();
	public void start();
	public void pause();
	public void resume();
	public void end();
	public void reset();
	
	public static final GameState DUMMY_STATE = new GameState() {
		
		@Override
		public void unregister(GameStateAware gameStateAware) {
			throw new IllegalStateException("This method should not be called on a dummy state. Only isPaused is supported");
		}
		
		@Override
		public void start() {
			throw new IllegalStateException("This method should not be called on a dummy state. Only isPaused is supported");
		}
		
		@Override
		public void resume() {
			throw new IllegalStateException("This method should not be called on a dummy state. Only isPaused is supported");
		}
		
		@Override
		public void reset() {
			throw new IllegalStateException("This method should not be called on a dummy state. Only isPaused is supported");
		}
		
		@Override
		public void register(GameStateAware gameStateAware) {
			throw new IllegalStateException("This method should not be called on a dummy state. Only isPaused is supported");
		}
		
		@Override
		public void pause() {
			throw new IllegalStateException("This method should not be called on a dummy state. Only isPaused is supported");
		}
		
		@Override
		public boolean isPaused() {
			return true;
		}
		
		@Override
		public void init() {
			throw new IllegalStateException("This method should not be called on a dummy state. Only isPaused is supported");
			
		}
		
		@Override
		public void end() {
			throw new IllegalStateException("This method should not be called on a dummy state. Only isPaused is supported");
		}
	};
}
