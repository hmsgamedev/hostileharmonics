package com.basildsouza.game.hostileharmonics.states;

import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.GameContainer;

public class InGameState implements GameState{
	private final List<GameStateAware> gameStateListeners = new LinkedList<GameStateAware>();
	private final GameContainer container;
	
	public InGameState(GameContainer container) {
		this.container = container;
	}
	
	@Override
	public void register(GameStateAware gameStateAware) {
		gameStateListeners.add(gameStateAware);
	}

	@Override
	public void unregister(GameStateAware gameStateAware) {
		gameStateListeners.remove(gameStateAware);
	}

	@Override
	public void init() {
		for(GameStateAware gameStateListener : gameStateListeners){
			gameStateListener.init(this);
		}
	}

	@Override
	public void start() {
		for(GameStateAware gameStateListener : gameStateListeners){
			gameStateListener.start();
		}
	}

	@Override
	public void pause() {
		container.pause();
		for(GameStateAware gameStateListener : gameStateListeners){
			gameStateListener.pause();
		}
	}

	@Override
	public void resume() {
		for(GameStateAware gameStateListener : gameStateListeners){
			gameStateListener.resume();
		}
		container.resume();
	}

	@Override
	public void end() {
		for(GameStateAware gameStateListener : gameStateListeners){
			gameStateListener.end();
		}
	}

	@Override
	public void reset() {
		for(GameStateAware gameStateListener : gameStateListeners){
			gameStateListener.reset();
		}
		//TODO: Call reinit here?
	}

	@Override
	public boolean isPaused() {
		return container.isPaused();
	}
}
