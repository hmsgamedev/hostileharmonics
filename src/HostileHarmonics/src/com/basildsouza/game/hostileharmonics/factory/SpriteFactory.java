package com.basildsouza.game.hostileharmonics.factory;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

import com.artemis.utils.Timer;
import com.basildsouza.game.hostileharmonics.component.Sprite;
import com.basildsouza.game.hostileharmonics.component.TextSprite;
import com.basildsouza.game.hostileharmonics.component.TextSprite.TextProvider;
import com.basildsouza.game.hostileharmonics.manager.BookKeeper;
import com.basildsouza.game.hostileharmonics.manager.BookKeeper.Book;
import com.basildsouza.tools.music.piano.engine.Accidental;
import com.basildsouza.tools.music.piano.engine.Clef;
import com.basildsouza.tools.music.piano.engine.Tone;

public class SpriteFactory {
	private final String imageRoot;
	
	private static final String BAR_LEFT = "bar_left_20x30.png";
	private static final String BAR_MIDDLE = "bar_middle_20x30.png";
	private static final String BAR_RIGHT = "bar_right_20x30.png";
	
	private static final String NOTE_QUARTER_STEM_UP = "note_quarter_stem_up_20x30.png";
	private static final String NOTE_QUARTER_STEM_DOWN = "note_quarter_stem_down_20x30.png";
	
	private static final String CHORD_INDIC_SHADOW = "model_plaque_shadow.png";
	private static final String CHORD_INDIC_EMPTY = "model_plaque.png";
	
	private static final String ACCIDENTAL_SHARP = "accidental_sharp_7x20.png";
	private static final String ACCIDENTAL_FLAT = "accidental_flat_7x20.png";
	private static final String[] ACCIDENTAL_ARRAY;
	
	private static final String TREBLE_CLEF = "treble_clef_20x50.png";
	private static final String BASS_CLEF = "bass_clef_20x50.png";
	private static final String[] CLEF_ARRAY;
	
	private static final int SPRITE_BAR_WIDTH = 20;
	private static final int SPRITE_BAR_HEIGHT = 30;
	
	private static final int SPRITE_CLEF_OFFSET = 9;
	private static final int SPRITE_QTR_NOTE_WIDTH = SPRITE_BAR_WIDTH;
	private static final int SPRITE_QTR_NOTE_HEIGHT = SPRITE_BAR_HEIGHT * 2;
	private static final int SPRITE_QTR_NOTE_HEIGHT_OFFSET = SPRITE_BAR_HEIGHT / 2;
	
	
	
	
	static {
		ACCIDENTAL_ARRAY = new String[Accidental.values().length];
		ACCIDENTAL_ARRAY[Accidental.Sharp.ordinal()] = ACCIDENTAL_SHARP;
		ACCIDENTAL_ARRAY[Accidental.Flat.ordinal()] = ACCIDENTAL_FLAT;
		
		CLEF_ARRAY = new String[Clef.values().length];
		CLEF_ARRAY[Clef.Treble.ordinal()] = TREBLE_CLEF; 
		CLEF_ARRAY[Clef.Bass.ordinal()]   = BASS_CLEF; 
	}
	
	//Note: WIP, need to add more, dont hard code anywhere!
	public enum Layer {
		Background, Player, HUD_Back, HUD_Front 
	}
	
	public SpriteFactory(String imageRoot) {
		this.imageRoot = imageRoot;
	}
	
	private static final Map<String, Image> imageFileCache = new HashMap<String, Image>(100);
	
	public Sprite createTimerSprite(final Timer timer) throws SlickException{
		final Font hudFont = loadHUDFont();
		Sprite sprite = new TextSprite(Layer.HUD_Front, new TextProvider() {
			@Override
			public String getText() {
				final float delay = Math.round(timer.getPercentageRemaining()*100);
				return String.valueOf(delay + "% Through");
			}
			
			@Override
			public Font getFont() {
				return hudFont;
			}
		});
				
		return sprite;
	}
	
	public Sprite createChordIndicatorShadowSprite() throws SlickException{
		return new Sprite(loadImage(CHORD_INDIC_SHADOW), Layer.HUD_Back);
	}
	
	public Sprite createChordIndicatorSprite(String mode, String chord) throws SlickException{
		final Image modeImage = loadImageNoCache(CHORD_INDIC_EMPTY);
		Graphics g = modeImage.getGraphics();
		Font hudBigFont = loadHUDFont(8);
		Font hudSmallFont = loadHUDFont(7);
		
		//TODO: Improve placement and sizes
		g.setFont(hudBigFont);
		int width = hudBigFont.getWidth(mode);
		g.drawString(mode, 25 - width/2, 7);
		g.setFont(hudSmallFont);
		width = hudSmallFont.getWidth(chord);
		g.drawString(chord, 46 - width, 16);
		g.flush();
		return new Sprite(modeImage, Layer.HUD_Back);
	}

	
	public Sprite createScoreLeftSprite(final Book book, int width, int height) throws SlickException{
		final Font hudFont = loadHUDFont();
		Sprite sprite = new TextSprite(Layer.HUD_Back, new TextProvider() {
			@Override
			public String getText() {
				return String.valueOf(Math.round(book.getResourceCount()*100)/100.00);
			}
			
			@Override
			public Font getFont() {
				return hudFont;
			}
		});
		
		sprite.setImage(createRectangle(width, height, Color.white));
				
		return sprite;
	}
	
	public Sprite createScoreRightSprite(final Book book, int width, int height) throws SlickException{
		final Font hudFont = loadHUDFont();
		Sprite sprite = new TextSprite(Layer.HUD_Back, new TextProvider() {
			@Override
			public String getText() {
				return String.valueOf(Math.round(book.getResourceCount()*100)/100.00);
			}
			
			@Override
			public Font getFont() {
				return hudFont;
			}
		});
		
		sprite.setImage(createRectangle(width, height, Color.white));
				
		return sprite;
	}
	
	public Sprite createScoreMidSprite(int width, int height) throws SlickException{
		//TODO: Show who is winning up here
		return new Sprite(createRectangle(width, height, Color.white), Layer.HUD_Back);
	}
	
	public Image createRectangle(int width, int height, int firstPart, 
								 Color color1, Color color2) throws SlickException{
		Image image = new Image(width, height);
		Graphics graphics = image.getGraphics();
		graphics.setColor(color1);
		graphics.fillRect(0, 0, firstPart, height);
		
		graphics.setColor(color2);
		graphics.fillRect(firstPart, 0, width, height);

		graphics.flush();
		
		return image;
	}
	
	public Image createRectangle(int width, int height, Color color) throws SlickException{
		return createRectangle(width, height, width, color, Color.black);
	}
	
	public Sprite createMultiplierSprite(final BookKeeper bookKeeper) throws SlickException{
		final Font hudFont = loadHUDFont();
		return new TextSprite(Layer.HUD_Front, new TextProvider() {
			@Override
			public String getText() {
				return "Multiplier\n" + Math.round(bookKeeper.getMultiplier()*100)/100.00;
			}
			
			@Override
			public Font getFont() {
				return hudFont;
			}
		});
	}
	
	public Sprite createScoreSprite(final Book score) throws SlickException{
		final Font hudFont = loadHUDFont();

		return new TextSprite(Layer.HUD_Front, new TextProvider() {
			@Override
			public String getText() {
				return score.getName() + "\n" + Math.round(score.getResourceCount()*100)/100.00;
			}
			
			@Override
			public Font getFont() {
				return hudFont;
			}
		});
	}
	
	public Sprite createBarSprite(Clef clef, int barWidth) throws SlickException{
		final Image barLeft = loadImage(BAR_LEFT);
		final Image barRight = loadImage(BAR_RIGHT);
		final Image barMiddle = loadImage(BAR_MIDDLE);
		final Image clefImage = loadImage(CLEF_ARRAY[clef.ordinal()]);

		/*
		if(barLeft.getWidth() != barRight.getWidth() && 
		   barLeft.getWidth() != barMiddle.getWidth() &&
		   barLeft.getHeight() != barRight.getHeight() && 
		   barLeft.getHeight() != barMiddle.getHeight()){
			
			// Perhaps throw exception?
			//throw new IllegalStateException("All sprite widths should be equal legngh)
		}*/
		
		int height = Math.max(barMiddle.getHeight(), 
							  Math.max(barLeft.getHeight(), barRight.getHeight()));
		int width = (barWidth+1) * barMiddle.getWidth();
		
		int indivWidth = barMiddle.getWidth();
		
		final Image overallBar = new Image(width+1, clefImage.getHeight());
		Graphics g = overallBar.getGraphics();
		
		g.drawImage(barLeft, 0, SPRITE_CLEF_OFFSET);
		g.drawImage(clefImage, 0, 0);
		
		for(int i=1;i<barWidth-1;i++){
			g.drawImage(barMiddle, i*indivWidth, SPRITE_CLEF_OFFSET);
		}
		g.drawImage(barRight, (barWidth-1)*indivWidth, SPRITE_CLEF_OFFSET);
		g.flush();
		return new Sprite(overallBar, Layer.HUD_Back);
	}
	
	private static final String[] NOTE_SPRITE_INDEX = {NOTE_QUARTER_STEM_UP, NOTE_QUARTER_STEM_DOWN};
	
	// TODO: Convert some 0s to one to select the other sprite once created
	private static final int[][] offsetPlus5 =
	//    Off  S  Le       Tr Bs       
		{{-21, 0, -2}, // G3 B1
		 {-18, 0, -2}, // A3 C2
		 {-14, 0, -1}, // B3 D2
		 {-11, 0, -1}, // C4 E2
		 { -7, 0,  0}, // D4 F2
		 { -3, 0,  0}, // E4 G2
		 {  0, 0,  0}, // F4 A2 -- Top Normal 
		 {  4, 0,  0}, // G4 B2
		 {  7, 0,  0}, // A4 C3
		 { 11, 0,  0}, // B4 D3
		 { -7, 1,  0}, // C5 E3
		 { -4, 1,  0}, // D5 F3
		 {  0, 1,  0}, // E5 G3 -- Down Normal
		 {  3, 1,  0}, // F5 A3
		 {  7, 1,  0}, // G5 B3
		 { 11, 1,  1}, // A5 C4
		 { 14, 1,  1}, // B5 D4
		 { 18, 1,  2}, // C6 E4
		 }; 
	
	//TODO: This will have to be varied later when we need to accomodate bigger sprite images
	//private static final int SPRITE_NOTE_0_POS = 0;
	
	private int[] getNoteProperties(int offset){
		return offsetPlus5[offset + 5];
	}
	
	private Map<String, Image> noteImageCache = new HashMap<String, Image>(127);
	
	public Sprite createQuaterNoteSprite(Clef clef, Tone tone) throws SlickException{
		final int[] noteProperties = getNoteProperties(clef.getOffset(tone));
		final int offset = noteProperties[0];
		
		final String sprite = NOTE_SPRITE_INDEX[noteProperties[1]];
		final int ledgerLines = noteProperties[2];
		final Accidental shift = tone.getNote().getShift();
		
		
		final String key = offset + "-" + sprite + "-" + shift;
		
		Image noteImage = noteImageCache.get(key);
		if(noteImage == null){
			Image tempImage = loadImage(sprite);
			noteImage = new Image(SPRITE_QTR_NOTE_WIDTH, SPRITE_QTR_NOTE_HEIGHT);
			Graphics g = noteImage.getGraphics();
			
			if(ledgerLines != 0){
				// Draw Ledger Lines
				drawLedgerLines(g, ledgerLines);
			}
			
			if(shift != Accidental.Natural){
				// Draw Accidentals
				final Image accidentalImage = loadImage(ACCIDENTAL_ARRAY[shift.ordinal()]);
				if(sprite == NOTE_QUARTER_STEM_UP){
					// Should not take full offset (some leeway to avoid a cuttoff note) . Very low priority
					g.drawImage(accidentalImage, 0, 15+SPRITE_QTR_NOTE_HEIGHT_OFFSET-offset+0);//SPRITE_CLEF_OFFSET); 
				} else if (sprite == NOTE_QUARTER_STEM_DOWN) {
					// Should not take full offset (some leeway to avoid a cuttoff note) . Very low priority
					g.drawImage(accidentalImage, 3, -6+SPRITE_QTR_NOTE_HEIGHT_OFFSET-offset+0);//SPRITE_CLEF_OFFSET); 
				}
			}
			
			g.drawImage(tempImage, 0, SPRITE_QTR_NOTE_HEIGHT_OFFSET - offset + 0);//SPRITE_CLEF_OFFSET);
			g.flush();
			
			Image finalImage = new Image(SPRITE_QTR_NOTE_WIDTH, SPRITE_QTR_NOTE_HEIGHT+SPRITE_CLEF_OFFSET);
			g = finalImage.getGraphics();
			g.drawImage(noteImage, 0, SPRITE_CLEF_OFFSET);
			g.flush();

			noteImage = finalImage;
			noteImageCache.put(key, noteImage);
		}
		
		return new Sprite(noteImage, Layer.HUD_Front);
	}

	private void drawLedgerLines(Graphics g, final int ledgerLines)
													throws SlickException {
		// Reassignment to remove compiler warning
		int test = SPRITE_BAR_HEIGHT;
		if(test != 30){
			// This doesnt really mean it is spoilt, but needs rechecking
			throw new IllegalStateException("Sprite height changed, Check if this code will still work before trying");
		}
		int numLedgerLines = Math.abs(ledgerLines);
		
		// This maths will breakdown 
		final int copyFromStart;
		final int copyFromEnd;
		final int copyToStart;
		final int copyToEnd;
		final int xOffset;
		
		if(ledgerLines < 0){
			// Bottom Ledger
			copyFromStart = 2;
			copyFromEnd = numLedgerLines * 8;
			copyToStart = SPRITE_QTR_NOTE_HEIGHT_OFFSET + SPRITE_BAR_HEIGHT + 1;
			copyToEnd = copyToStart+copyFromEnd-copyFromStart;
			xOffset = 0;
		} else {
			// Top Ledger
			copyFromStart = (numLedgerLines-1) * 7;
			copyFromEnd =  13;
			copyToStart = 0;
			copyToEnd = copyToStart+copyFromEnd-copyFromStart;
			xOffset = 2;
		}
		
		g.drawImage(loadImage(BAR_MIDDLE), 
				xOffset + 6, // x Final image to start drawing - always static
				copyToStart, // Y to start drawing
				xOffset+SPRITE_BAR_WIDTH-2, // x final image ot end drawing 
				copyToEnd,// Y to stop drawing
				xOffset+6, // X Src to start copying - always static  
				copyFromStart, // Y to start copying
				xOffset+SPRITE_BAR_WIDTH-2, // X Limit to draw till.
				copyFromEnd); // Y limit to draw (for 2)
	}

	private Image loadImage(String imagePath) throws SlickException{
		Image image = imageFileCache.get(imagePath);
		if(image == null){
			image = loadImageNoCache(imagePath);
			imageFileCache.put(imagePath, image);
		}
		return image;
	}
	
	private Image loadImageNoCache(String imagePath) throws SlickException{
		return new Image(imageRoot + imagePath);
	}
	
	private Font loadHUDFont() throws SlickException{
		return loadHUDFont(12);
	}
	
	@SuppressWarnings("unchecked")
	private Font loadHUDFont(int size) throws SlickException{
		final UnicodeFont font = new UnicodeFont(new java.awt.Font("Verdana", java.awt.Font.BOLD, size));
		font.addAsciiGlyphs();
		font.getEffects().add(new ColorEffect(java.awt.Color.black));
		font.loadGlyphs();
		
		return font;
	}


}
