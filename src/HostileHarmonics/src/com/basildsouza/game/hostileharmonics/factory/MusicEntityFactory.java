package com.basildsouza.game.hostileharmonics.factory;

import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Float;

import org.newdawn.slick.SlickException;

import com.artemis.Entity;
import com.artemis.World;
import com.basildsouza.game.hostileharmonics.component.Position;
import com.basildsouza.game.hostileharmonics.component.ToneSequence;
import com.basildsouza.game.hostileharmonics.component.Velocity;
import com.basildsouza.game.hostileharmonics.utils.BarHelper;
import com.basildsouza.tools.music.piano.engine.Clef;
import com.basildsouza.tools.music.piano.engine.Tone;

public class MusicEntityFactory {
	
	private final SpriteFactory spriteFactory;

	public MusicEntityFactory(SpriteFactory spriteFactory) {
		this.spriteFactory = spriteFactory;
	}
	
	public Entity createBarStaff(World world, BarHelper barHelper, Clef clef) throws SlickException{
		final Entity barStaff = world.createEntity();
		
		Float barPosition = barHelper.getBarPosition(clef);
		barStaff.addComponent(new Position(barPosition.x, barPosition.y));
		barStaff.addComponent(spriteFactory.createBarSprite(clef, barHelper.getBarCapacity() + barHelper.getInitialSymbolWidth()));
		
		return barStaff;
	}
	
	public Entity createTone(World world, BarHelper barHelper, 
							 Clef clef, Tone tone) throws SlickException{
		// Here it should create based on an offset from the bar location y - the extra note height
		// For xPos also, it should just know how many notes away from the start it is
		final Entity toneEntity = world.createEntity();
		Point2D.Float position = barHelper.getPosForLastIndex(clef);
		
		toneEntity.addComponent(new Position(position.x, position.y));
		toneEntity.addComponent(new ToneSequence(clef, tone));
		toneEntity.addComponent(spriteFactory.createQuaterNoteSprite(clef, tone));
		toneEntity.addComponent(new Velocity());
		
		return toneEntity;
	}
}
