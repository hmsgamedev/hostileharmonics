package com.basildsouza.game.hostileharmonics.factory;

import java.awt.geom.Point2D;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import com.artemis.ComponentType;
import com.artemis.Entity;
import com.artemis.World;
import com.artemis.utils.Timer;
import com.basildsouza.game.hostileharmonics.TempEndGameState;
import com.basildsouza.game.hostileharmonics.TonePlayMode;
import com.basildsouza.game.hostileharmonics.component.Position;
import com.basildsouza.game.hostileharmonics.component.Sprite;
import com.basildsouza.game.hostileharmonics.component.Timed;
import com.basildsouza.game.hostileharmonics.manager.BookKeeper;
import com.basildsouza.game.hostileharmonics.manager.BookKeeper.Book;
import com.basildsouza.game.hostileharmonics.system.ScoreBarSystem;
import com.basildsouza.tools.music.piano.engine.chord.Chord;

public class HUDEntityFactory {
	private final SpriteFactory spriteFactory;

	public HUDEntityFactory(SpriteFactory spriteFactory) {
		this.spriteFactory = spriteFactory; 
	}
	
	public Map<Chord, Entity> createChordIndicators(World world, 
									  List<Entry<Chord, TonePlayMode>> tonePlayModeEntries, 
									  Point2D.Float startPos) throws SlickException{
		
		Collections.sort(tonePlayModeEntries, new Comparator<Entry<Chord, TonePlayMode>>() {
			@Override
			public int compare(Entry<Chord, TonePlayMode> o1,
					Entry<Chord, TonePlayMode> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		});
		
		final Sprite shadowSprite = spriteFactory.createChordIndicatorShadowSprite();
		final Map<Chord, Entity> indicatorChordMap = new HashMap<Chord, Entity>();
		
		int indicatorNum = 0;
		for(Entry<Chord, TonePlayMode> tonePlayModeEntry : tonePlayModeEntries){
			final Chord chord = tonePlayModeEntry.getKey();
			final String chordName = chord.getChordName();
			final String modeName = tonePlayModeEntry.getValue().getName();
			
			final Entity shadowEntity = world.createEntity();
			shadowEntity.addComponent(new Position(startPos.x+indicatorNum*55, startPos.y + 40));
			shadowEntity.addComponent(shadowSprite);
			shadowEntity.addToWorld();
			
			
			final Entity indicatorEntity = world.createEntity();
			indicatorEntity.addComponent(new Position(startPos.x+indicatorNum*55, startPos.y));
			indicatorEntity.addComponent(spriteFactory.createChordIndicatorSprite(modeName, chordName));
			indicatorEntity.addToWorld();
			
			indicatorChordMap.put(chord, indicatorEntity);
			indicatorNum ++;
		}
		
		return indicatorChordMap;
	}
	
	public ScoreBarSystem createMainScoreBarSystem(World world, Book p1Book, Book p2Book,
												   Color p1Color, Color p2Color,
												   Point2D.Float position, int width, 
												   StateBasedGame game) throws SlickException{
		if(width % 10 != 0){
			throw new IllegalStateException("Width should be divisible by 10");
		}
		int indivWidth = width / 10;
		final int height = 30;
		Sprite[] entitySprites = createScoreBarEntities(world, p1Book, p2Book, position, width/10, height);
		Image[] scoreImages = new Image[11];
		int i=0;
		scoreImages[0] = spriteFactory.createRectangle(indivWidth, height, p2Color);
		for(i=1; i<10;i++){
			int firstPart = (int) Math.round(indivWidth * (i/10.00));
			scoreImages[i] = spriteFactory.createRectangle(indivWidth, height, firstPart, p1Color, p2Color); 
		}
		scoreImages[i] = spriteFactory.createRectangle(indivWidth, height, p1Color);
		
		
		final Timer gameTimer = createGameTimer(world, 5 * 60 * 1000, 370, 50, game); 

		System.out.println(gameTimer);
		final ScoreBarSystem scoreBar 
				= new ScoreBarSystem(p1Book, p2Book, scoreImages, entitySprites);
		
		return scoreBar;
	}
	
	//TODO: Find a proper structure for this
	public Timer createGameTimer(World world, float amount, int xPos, int yPos, final StateBasedGame game) throws SlickException{
		//TODO: Make a derived class
		final Timer gameTimer = new Timer(amount) {
			
			@Override
			public void execute() {
				//TODO: End the game eventually
				game.enterState(TempEndGameState.GAME_STATE_ID, new FadeOutTransition(), new FadeInTransition());
				
			}
		};
System.out.println(gameTimer.getDelay() + " " + gameTimer.getPercentageRemaining() + " " + gameTimer.isRunning());
		
		
		Entity timerEntity = world.createEntity();
		timerEntity.addComponent(new Position(xPos, yPos));
		timerEntity.addComponent(spriteFactory.createTimerSprite(gameTimer), ComponentType.getTypeFor(Sprite.class));
		timerEntity.addComponent(new Timed(gameTimer));
		timerEntity.addToWorld();
		
		return gameTimer;
	}
	
	
	
	public Sprite[] createScoreBarEntities(World world, Book p1Book, Book p2Book, 
										   Point2D.Float position, int width, int height) 
												   						throws SlickException{
		
		final Sprite[] spriteComponent = new Sprite[10];
		final int indivWidth = width;
		int i=0;
		
		Entity scoreStartEntity = world.createEntity();
		scoreStartEntity.addComponent(new Position(position.x+i*indivWidth, position.y));
		spriteComponent[i] = spriteFactory.createScoreLeftSprite(p1Book, indivWidth, height);
		scoreStartEntity.addComponent(spriteComponent[i], ComponentType.getTypeFor(Sprite.class));
		scoreStartEntity.addToWorld();

		for(i=1; i<9; i++){
			Entity scoreEntity = world.createEntity();
			scoreEntity.addComponent(new Position(position.x+i*indivWidth, position.y));
			spriteComponent[i] = spriteFactory.createScoreMidSprite(indivWidth, height);
			scoreEntity.addComponent(spriteComponent[i], ComponentType.getTypeFor(Sprite.class));
			scoreEntity.addToWorld();
		}
		
		Entity scoreEndEntity = world.createEntity();
		scoreEndEntity.addComponent(new Position(position.x+i*indivWidth, position.y));
		spriteComponent[i] = spriteFactory.createScoreRightSprite(p2Book, indivWidth, height);
		scoreEndEntity.addComponent(spriteComponent[i], ComponentType.getTypeFor(Sprite.class));
		scoreEndEntity.addToWorld();
		
		return spriteComponent;
	}

	public void createScoreEntities(World world, List<Book> books, int startXPos, int yPos) throws SlickException{
		int scoreNum = 0;
		for(Book book : books){
			final int xPos = startXPos + scoreNum*60 + 100;

			createScoreEntity(world, book, xPos, 200).addToWorld();
			
			scoreNum++;
		}
	}

	public Entity createScoreEntity(World world, Book book, int xPos, int yPos) throws SlickException{
		final Sprite scoreSprite = spriteFactory.createScoreSprite(book);
		final Position scorePos = new Position(xPos, yPos);
		
		final Entity score = world.createEntity();
		score.addComponent(scoreSprite, ComponentType.getTypeFor(Sprite.class));
		score.addComponent(scorePos);
		
		return score;
	}
	
	public Entity createMultiplierEntity(World world, BookKeeper bookKeeper, int xPos, int yPos) throws SlickException{
		final Sprite scoreSprite = spriteFactory.createMultiplierSprite(bookKeeper);
		final Position scorePos = new Position(xPos, yPos);
		
		final Entity score = world.createEntity();
		score.addComponent(scoreSprite, ComponentType.getTypeFor(Sprite.class));
		score.addComponent(scorePos);
		
		return score;
	}
}
