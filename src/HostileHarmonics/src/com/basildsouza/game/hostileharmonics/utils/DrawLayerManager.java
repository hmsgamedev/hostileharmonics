package com.basildsouza.game.hostileharmonics.utils;

import org.newdawn.slick.Graphics;

import com.artemis.utils.Bag;
import com.basildsouza.game.hostileharmonics.component.Position;
import com.basildsouza.game.hostileharmonics.component.Sprite;

//TODO: Consider making this more efficient later
public class DrawLayerManager {
	private final Bag<Pair<Position, Sprite>>[] layers;
	
	@SuppressWarnings("unchecked")
	public DrawLayerManager(int numLayers) {
		this.layers = new Bag[numLayers];
		for(int i=0; i<layers.length; i++){
			//TODO: Size this based on expectations per layer if required?
			layers[i] = new Bag<Pair<Position,Sprite>>(200);
		}
	}
	
	public void addSprite(Position position, Sprite sprite){
		// Should I check if the layer exists? nah, speed over safety :)
		layers[sprite.getLayer().ordinal()].add(new Pair<Position, Sprite>(position, sprite));
	}
	
	//Note: Perhaps in the future convert this to render per layer?
	public void renderLayers(Graphics g, int xOffset, int yOffset){
		for(int i=0; i<layers.length; i++){
			for(int j=0; j<layers[i].size(); j++){
				final Pair<Position, Sprite> spritePair = layers[i].get(j);
				final Position position = spritePair.getFirst();
				final Sprite sprite = spritePair.getSecond();
				
				sprite.render(g, position.getXPos()+xOffset, 
							     position.getYPos()+yOffset);
			}
		}
	}
	
	public void clear(){
		for(int i=0; i<layers.length; i++){
			layers[i].clear();
		}
	}
}
