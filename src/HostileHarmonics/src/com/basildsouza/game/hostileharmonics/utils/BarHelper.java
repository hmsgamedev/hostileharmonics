package com.basildsouza.game.hostileharmonics.utils;

import java.awt.geom.Point2D;

import com.basildsouza.tools.music.piano.engine.Clef;

public class BarHelper {
	private final Point2D.Float[] barPosition;
	
	private final String playerID;
	private final int numNotes;
	private final int barHeightPx;
	private final int noteWidthPx;
	private final int noteStartOffset;
	
	//TODO: Eventually take a lesser collection of data and interpret them here to make 2 clefs
	public BarHelper(final Point2D.Float trebleClefPosition, 
					 final Point2D.Float bassClefPosition, 
					 final String playerID, 
					 final int numNotes, final int noteWidth, 
					 final int noteStartOffset) {
		this.barPosition = new Point2D.Float[]{trebleClefPosition, bassClefPosition};
		this.playerID = playerID;
		this.numNotes = numNotes;
		this.noteWidthPx = noteWidth;
		this.noteStartOffset = noteStartOffset;
		this.barHeightPx = 30;
	}
	
	public Point2D.Float getBarPosition(Clef clef) {
		return barPosition[clef.ordinal()];
	}
	
	public int getNoteStartOffset() {
		return noteStartOffset;
	}
	
	public int getBarCapacity() {
		return numNotes;
	}
	
	public int getInitialSymbolWidth(){
		return 1; //TODO: Perhaps make it configurable later
	}
	
	public int getNoteWidth() {
		return noteWidthPx;
	}
	
	public String getPlayerID() {
		return playerID;
	}

	public Point2D.Float getPosForSecondLastIndex(Clef clef){
		return getPosForIndex(clef, numNotes-2);
	}

	public Point2D.Float getPosForLastIndex(Clef clef){
		return getPosForIndex(clef, numNotes-1);
	}
	
	public Point2D.Float getPosForIndex(Clef clef, int index){
		return new Point2D.Float(noteStartOffset + index*noteWidthPx + barPosition[clef.ordinal()].x, 
								 barPosition[clef.ordinal()].y-barHeightPx/2);
	}
}