package com.basildsouza.game.hostileharmonics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.basildsouza.game.hostileharmonics.manager.BookKeeper;
import com.basildsouza.game.hostileharmonics.manager.BookKeeper.Book;
import com.basildsouza.game.hostileharmonics.utils.BarHelper;
import com.basildsouza.tools.music.piano.engine.ToneChecker;
import com.basildsouza.tools.music.piano.engine.chord.Chord;
import com.basildsouza.tools.music.piano.engine.event.PlayerDevice;
import com.basildsouza.tools.music.piano.engine.generator.TuneGenerator;

//TODO: This should be loaded from file for some parts, and menu screens for other parts
public class PlayerConfig {
	private String playerName;
	private BarHelper barHelper;
	private TuneGenerator toneSequenceGenerator;
	private ToneChecker toneTransposer;
	private BookKeeper bookKeeper;
	
	private Map<Chord, TonePlayMode> tonePlayMap;
	private Chord defaultChord;
	
	private PlayerDevice playerDevice;
	private Book scoreBook;
	
	public PlayerConfig(String playerName, BarHelper barHelper, PlayerDevice playerDevice, 
						TuneGenerator toneSequenceGenerator, ToneChecker toneTransposer) {
		this.playerName = playerName;
		this.barHelper = barHelper;
		this.playerDevice = playerDevice;
		this.toneSequenceGenerator = toneSequenceGenerator;
		this.toneTransposer = toneTransposer;
	}
	
	public List<Entry<Chord, TonePlayMode>> getTonePlayEntries(){
		return new ArrayList<Map.Entry<Chord,TonePlayMode>>(tonePlayMap.entrySet());
	}
	
	public TuneGenerator getToneSequenceGenerator() {
		return toneSequenceGenerator;
	}
	
	public ToneChecker getToneTransposer() {
		return toneTransposer;
	}
	
	public BarHelper getBarHelper() {
		return barHelper;
	}
	
	public PlayerDevice getPlayerDevice() {
		return playerDevice;
	}
	
	public String getPlayerName() {
		return playerName;
	}
	
	public BookKeeper getBookKeeper() {
		return bookKeeper;
	}
	
	public void setBookKeeper(BookKeeper bookKeeper) {
		this.bookKeeper = bookKeeper;
	}
	
	public void setTonePlayMap(Map<Chord, TonePlayMode> tonePlayMap) {
		this.tonePlayMap = tonePlayMap;
	}
	
	public void setDefaultChord(Chord defaultChord) {
		this.defaultChord = defaultChord;
	}
	
	public Chord getDefaultChord() {
		return defaultChord;
	}
	
	public TonePlayMode getDefaultTonePlayMode() {
		return tonePlayMap.get(defaultChord);
	}
	
	public TonePlayMode getTonePlayModeFor(Chord chord) {
		return tonePlayMap.get(chord);
	}

	public void setScoreBook(Book scoreBook) {
		this.scoreBook = scoreBook;
	}
	
	public Book getScoreBook() {
		return scoreBook;
	}
}