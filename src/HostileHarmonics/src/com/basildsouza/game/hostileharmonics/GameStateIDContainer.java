package com.basildsouza.game.hostileharmonics;

public interface GameStateIDContainer {
	int IN_GAME_STATE = 1;
	int TEMP_MIDI_SELECTOR = 2;
	int LOADING_STATE = 3;
	int TEMP_END_GAME_STATE = 4;
}
