package com.basildsouza.game.hostileharmonics;

import org.newdawn.slick.state.StateBasedGame;

public class GameStateManager {
	private static GameStateManager instance;
	
	public static GameStateManager createInstance(StateBasedGame game) {
		return instance=new GameStateManager(game);
	}

	public static GameStateManager getInstance() {
		return instance;
	}
	
	private StateBasedGame game;
	//Perhaps container here as wells
	
	public GameStateManager(StateBasedGame game) {
		this.game = game;
	}
	
	public boolean isPaused(){
		// This can later be made more elaborate
		return game.getCurrentStateID() == InGamePlayState.GAME_STATE_ID;
	}
}
