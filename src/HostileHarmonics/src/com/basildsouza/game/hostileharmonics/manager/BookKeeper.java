package com.basildsouza.game.hostileharmonics.manager;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.basildsouza.game.hostileharmonics.utils.Pair;

/**
 * The Book Keeper will keep track of the resources available 
 * for the player as well as answering questions about if the 
 * player has enough resources to perform actions.
 * 
 * This does not inherit from a system, but for lack of a better palce to put it, it is here
 * @author Basil Dsouza
 *
 */
public class BookKeeper {
	public static final int BASE_SCORE = 0;
	
	private final Map<String, Book> bookMap = new LinkedHashMap<String, BookKeeper.Book>(10);
	private List<Book> activeBooks;

	private final Multiplier globalMultiplier;
	
	public BookKeeper() {
		globalMultiplier = createDefaultMultiplier();
		
		//TODO: Accept a reference to the world, 
		// so that when the multiplier increases, 
		//it should be capable of adding an entity 
	}
	
	public double getMultiplier(){
		return globalMultiplier.getMultiplier();
	}
	
	public List<Book> getAllBooks(){
		return new LinkedList<BookKeeper.Book>(bookMap.values());
	}
	
	public void correctActionPerformed(double difficulty){
		for(Book activeBook : activeBooks){
			activeBook.incrementResource(difficulty * (int)globalMultiplier.getMultiplier());
			//Later on increment each book's multiplier and check individually
		}
		final int oldMultiplier = (int) globalMultiplier.getMultiplier();
		globalMultiplier.increment();
		final int newMultiplier = (int) globalMultiplier.getMultiplier();
		checkForChange(oldMultiplier, newMultiplier);
	}
	
	public void wrongActionPerformed(){
		final int oldMultiplier = (int) globalMultiplier.getMultiplier();
		globalMultiplier.decrement();
		final int newMultiplier = (int) globalMultiplier.getMultiplier();
		checkForChange(oldMultiplier, newMultiplier);
		
		//TODO: Consider incrementing each books multiplier separately
//		for(Book activeBook : activeBooks){
//			activeBook.decrementMultiplier();
//		}
	}
	
	private void checkForChange(int oldMul, int newMul){
		if(oldMul != newMul){
			//pass in a boolean like this oldMul < newMul;
			//TODO: Insert special effects here
		}
	}
	
	public void setActive(Book... books){
		activeBooks = Arrays.asList(books);
	}
	
	private static Multiplier createDefaultMultiplier(){
		return new Multiplier(1, 1, 10, 0.1, 0.5);
	}
	
	public Book getBook(String bookName){
		return bookMap.get(bookName);
	}
	
	//TODO: Perhaps also add a flag for read only (so that no one modifies the global score object that affects the lower ones)
	public Book createMetaBook(String name, Pair<Book, Double>[] components){
		if(bookMap.containsKey(name)){
			throw new IllegalStateException("Cannot create 2 books with the same name");
		}
		final Book book = new MetaBook(name, components);
		bookMap.put(name, book);
		return book;
	}
	
	public Book createBook(String resourceName/*, Other details go here eventually */){
		if(bookMap.containsKey(resourceName)){
			throw new IllegalStateException("Cannot create 2 books with the same name");
		}
		//TODO: Currently all books have same settings for multipliers
		Multiplier multiplier = createDefaultMultiplier();
		final Book book = new BasicBook(resourceName, multiplier);
		bookMap.put(resourceName, book);
		return book;
	}
	
	public abstract class Book {
		private final String name;
		private final Multiplier multiplier;

		public Book(String name, Multiplier multiplier) {
			this.name = name;
			this.multiplier = multiplier;
		}
		
		public String getName() {
			return name;
		}
		public abstract double getResourceCount();
		public abstract boolean checkResourceAvailable(double amount);
		public abstract void useResource(double amount);
		
		protected abstract void incrementResource(double amount);
		
		public double getMultiplier() {
			return multiplier.getMultiplier();
		}
		
		protected void incrementMultiplier(){
			multiplier.increment();
		}
		
		protected void decrementMultiplier(){
			multiplier.decrement();
		}
	}
	
	private class MetaBook extends Book{
		private final Pair<Book,Double>[] components;

		public MetaBook(String name, Pair<Book, Double>[] components) {
			super(name, new Multiplier(1, 1, 1, 0, 0));
			
			this.components = components;
		}

		@Override
		public double getResourceCount() {
			double resourceCount = 0;
			for(Pair<Book, Double> component : components){
				resourceCount += component.getFirst().getResourceCount() * 
								 component.getSecond();
			}
			return resourceCount;
		}

		// TODO: Perhaps this is better potrayed as a cost object, which can also tell which resource is less
		// The book should not have this method at all?
		@Override
		public boolean checkResourceAvailable(double amount) {
			for(Pair<Book, Double> component : components){
				if(!component.getFirst().checkResourceAvailable(amount*component.getSecond())){
					return false;
				}
			}
			return true;
		}

		@Override
		public void useResource(double amount) {
			for(Pair<Book, Double> component : components){
				component.getFirst().useResource(amount * component.getSecond());
			}
		}

		@Override
		protected void incrementResource(double amount) {
			for(Pair<Book, Double> component : components){
				component.getFirst().incrementResource(amount * component.getSecond());
			}
		}
		
	}
	
	private class BasicBook extends Book {
		private double resourceCount;

		private BasicBook(String name, Multiplier multiplier) {
			super(name, multiplier);
		}
		
		public double getResourceCount() {
			return resourceCount;
		}
		
		protected void incrementResource(double amount){
			resourceCount += (amount * getMultiplier());
		}
		
		public boolean checkResourceAvailable(double amount){
			return resourceCount >= amount;
		}
		
		public void useResource(double amount){
			resourceCount -= amount;
		}
	}
	
	private static class Multiplier {
		private double multiplier;
		private final double minMultiplier;
		private final double maxMultiplier;
		private final double multiplierIncrements;
		private final double multiplierDecrements;

		//TODO: Add some form of decay calclation to decrement differently based on level
		private Multiplier(double initMultiplier, // This should normally be 1
					 double minMultiplier, // This can be less than 1 sometimes
					 double maxMultiplier, 
					 double multiplierIncrements, // For each right answer 
					 double multiplierDecrements // For each wrong answer reduce by
					 ) {
			this.minMultiplier = minMultiplier;
			this.maxMultiplier = maxMultiplier;
			this.multiplierIncrements = multiplierIncrements;
			this.multiplierDecrements = multiplierDecrements;
			this.multiplier = initMultiplier;
		}
		
		public double getMultiplier() {
			return multiplier;
		}
		
		private double increment(){
			return (multiplier = Math.min(maxMultiplier, multiplier+multiplierIncrements));
		}
		
		private double decrement(){
			return (multiplier = Math.max(minMultiplier, multiplier-multiplierDecrements));
		}

		
	}
	
}
