package com.basildsouza.game.hostileharmonics;

import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.basildsouza.game.hostileharmonics.manager.BookKeeper;
import com.basildsouza.game.hostileharmonics.manager.BookKeeper.Book;

public class TempEndGameState extends BasicGameState {
	public final static int GAME_STATE_ID = GameStateIDContainer.TEMP_END_GAME_STATE;
	private String winning;
	
	@Override
	public void enter(GameContainer container, StateBasedGame game)
			throws SlickException {
		// TODO Auto-generated method stub
		GameConfig config = GameConfig.getInstance();
		if(config.getNumPlayers() != 2){
			// Then this wont work!
		}
		PlayerConfig player1 = config.getConfigForPlayer(0);
		PlayerConfig player2 = config.getConfigForPlayer(1);
		
		double mainScore1 = player1.getBookKeeper().getBook("Score").getResourceCount();
		double mainScore2 = player2.getBookKeeper().getBook("Score").getResourceCount();
		
		
		if(mainScore1 < mainScore2){
			winning = "Player 2 Won!";
		} else if(mainScore2 < mainScore1){
			winning = "Player 1 Won!";
		} else {
			winning  = "Game Tied!";
		}
	}

	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		if(winning != null ){
			final UnicodeFont font = new UnicodeFont(new java.awt.Font("Verdana", java.awt.Font.BOLD, 36));
			font.addAsciiGlyphs();
			font.getEffects().add(new ColorEffect(java.awt.Color.red));
			font.loadGlyphs();
			
			Font oldFont = g.getFont();
			g.setFont(font);
			g.drawString(winning, 300, 200);
			
			g.setFont(oldFont);
		}

		
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
	}

	@Override
	public int getID() {
		return GAME_STATE_ID;
	}

}
