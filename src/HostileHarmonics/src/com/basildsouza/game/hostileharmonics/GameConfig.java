package com.basildsouza.game.hostileharmonics;

import java.util.LinkedList;
import java.util.List;

public class GameConfig {
	private static GameConfig instance = new GameConfig(); 
	
	private List<PlayerConfig> playerList;
	
	private GameConfig() {
		playerList = new LinkedList<PlayerConfig>();
	}
	
	public int getNumPlayers(){
		return playerList.size();
	}
	
	public PlayerConfig getConfigForPlayer(int playerNum){
		return playerList.get(playerNum);
	}
	
	//TODO: This should be in a different interface?
	public void addPlayer(PlayerConfig pc){
		playerList.add(pc);
	}
	
	public static GameConfig getInstance() {
		return instance;
	}
}
