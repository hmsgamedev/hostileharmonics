package com.basildsouza.game.hostileharmonics.system;

import java.awt.geom.Point2D.Float;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.annotations.Mapper;
import com.artemis.utils.ImmutableBag;
import com.basildsouza.game.hostileharmonics.HostileHarmonics;
import com.basildsouza.game.hostileharmonics.PlayerConfig;
import com.basildsouza.game.hostileharmonics.TonePlayMode;
import com.basildsouza.game.hostileharmonics.component.Destination;
import com.basildsouza.game.hostileharmonics.component.Position;
import com.basildsouza.game.hostileharmonics.component.ToneSequence;
import com.basildsouza.game.hostileharmonics.factory.MusicEntityFactory;
import com.basildsouza.game.hostileharmonics.states.GameState;
import com.basildsouza.game.hostileharmonics.states.GameStateAware;
import com.basildsouza.game.hostileharmonics.utils.BarHelper;
import com.basildsouza.tools.music.piano.engine.Clef;
import com.basildsouza.tools.music.piano.engine.Tone;
import com.basildsouza.tools.music.piano.engine.ToneChecker;
import com.basildsouza.tools.music.piano.engine.chord.Chord;
import com.basildsouza.tools.music.piano.engine.event.ChordEvent;
import com.basildsouza.tools.music.piano.engine.event.ChordListener;
import com.basildsouza.tools.music.piano.engine.event.ToneEvent;
import com.basildsouza.tools.music.piano.engine.event.ToneListener;
import com.basildsouza.tools.music.piano.engine.generator.TuneGenerator.GeneratedTone;

public class NoteGeneratorSystem 
	   extends EntitySystem 
	   implements GameStateAware, 
	   			ToneListener, ChordListener{ 
	//TODO: Ideally these chord and otne listeners should get direct tones and chords form the player conf, but life is short :)
	
	@Mapper
	ComponentMapper<ToneSequence> toneSeqMapper;
	
	@Mapper
	ComponentMapper<Position> positionMapper;

	private final BarHelper barHelper;
	
	private final AtomicInteger toneQIdx = new AtomicInteger(0);
	
	@SuppressWarnings("unchecked")
	private final List<ToneEvent>[] playedToneQueue 
				= new List[] {new ArrayList<ToneEvent>(20), 
							  new ArrayList<ToneEvent>(20)};
	
	// We will only consider a single latest chord input for a given game loop 
	private final AtomicReference<ChordEvent> chordEventHolder = new AtomicReference<ChordEvent>();
	private Chord currentChord;
	private TonePlayMode currentMode;
	
	private final Queue<Entity> entityQueue = new ArrayDeque<Entity>(100);

	private Entity latestEntity;

	private final MusicEntityFactory musicEntityFactory;

	private final ToneChecker toneTransposer;

	private final PlayerConfig playerConfig;

	private final Map<Chord, Entity> chordIndicators;
	
	private GameState gameState;
	
	private static final int CHORD_INDICATOR_RAISE_HEIGHT = 10;
	
	@SuppressWarnings("unchecked")
	public NoteGeneratorSystem(PlayerConfig playerConfig,
							   MusicEntityFactory musicEntityFactory, 
							   Map<Chord, Entity> chordIndicators) {
		super(Aspect.getAspectForAll(ToneSequence.class, Position.class));
		this.playerConfig = playerConfig;
		this.chordIndicators = chordIndicators;
		
		selectChord(playerConfig.getDefaultChord(), playerConfig.getDefaultTonePlayMode());
		
		this.musicEntityFactory = musicEntityFactory;
		this.barHelper = playerConfig.getBarHelper();
		this.toneTransposer = playerConfig.getToneTransposer();
		this.gameState = GameState.DUMMY_STATE;
	}
	
	@Override
	protected void initialize() {
		try {
			musicEntityFactory.createBarStaff(world, barHelper, Clef.Treble).addToWorld();
			musicEntityFactory.createBarStaff(world, barHelper, Clef.Bass).addToWorld();
			//createNewNote(0);
		} catch (SlickException e) {
			HostileHarmonics.printWithTimestamp("Something bad happened, coudlnt create original note");
			e.printStackTrace();
		}
	}
	
	private void removeFirstTone(){
		if(entityQueue.isEmpty()){
			return;
		}
		final Entity firstEntity = entityQueue.poll();
		firstEntity.deleteFromWorld();
		//TODO: 
		// Dont completely remove it 
		// Override deleted and then remove from pending list 
		// Dont move from here itself, instead only after the deleted.
		
		Entity lastEntry = null;
		int index = 0;
		for(final Entity remain : entityQueue){
			final Position position = positionMapper.get(remain);
			final ToneSequence toneSequence = toneSeqMapper.get(remain);
			final Clef clef = toneSequence.getClef();
			
			Float posForIndex = barHelper.getPosForIndex(clef, index);
			int howFar = (int) Math.ceil((position.getXPos()-posForIndex.getX())/barHelper.getNoteWidth());
			howFar = Math.max(howFar, 1);
			remain.addComponent(new Destination(posForIndex, 
												new Vector2f(5*howFar, 1), 2));
			remain.changedInWorld();
			lastEntry = remain;
			index++;
			
		}
		
		if(lastEntry != latestEntity){
			HostileHarmonics.printWithTimestamp("Something wrong? The latest entity should be equal " +
							   					"to last entry here. But wasnt!");
		}
	}
	
	private void createNewNote(int forIndex) throws SlickException{
		GeneratedTone nextTone = currentMode.getToneSequenceGenerator().getNextTone();
		Clef clef = nextTone.getClef();
		
		//TODO: Ensure that the newly generated tone is within the range of the player device.
		
		Entity newTone = musicEntityFactory.createTone(world, barHelper, clef, nextTone.getTone());
		Float posForIndex = barHelper.getPosForIndex(clef, forIndex);
		int howFar = (int) Math.ceil((barHelper.getPosForLastIndex(clef).getX()-posForIndex.getX())/
									  barHelper.getNoteWidth());
		howFar = Math.max(howFar, 1);
		
		
		newTone.addComponent(new Destination(posForIndex, 
											new Vector2f(5*howFar, 1), 5));
		
		latestEntity = newTone;
		entityQueue.offer(newTone);
		newTone.addToWorld();
	}
	
	private void addMoreNotesIfRequired() throws SlickException{
		if(entityQueue.size() >= barHelper.getBarCapacity()){
			return;
		}
		if(latestEntity != null){
			// The exact Clef doesnt matter, because we just want x post not Y.
			Float posForIndex = barHelper.getPosForSecondLastIndex(Clef.Treble);
			Position latestPosition = positionMapper.get(latestEntity);
			if(posForIndex.x < latestPosition.getXPos()){
				// Means that the last one has not yet reached the right place
				// So we will wait for a later time to create a new note
				return;
			}
		}
		createNewNote(entityQueue.size());
	}
	
	@Override
	protected void processEntities(ImmutableBag<Entity> entities) {
		final int oldToneIdx = toneQIdx.get();
		toneQIdx.set((toneQIdx.get() + 1) % 2);
		int numProcessed = 0;
		final List<ToneEvent> currToneList = playedToneQueue[oldToneIdx];
		for(ToneEvent te : currToneList){
			HostileHarmonics.printWithTimestamp(playerConfig.getPlayerName() + ": Processing Tone: " + te);
			final Entity first = entityQueue.peek();
			if(first == null){
				break;
			}
			final Tone playedTone = te.getPlayedTone();
			final Tone expectedTone = toneSeqMapper.get(first).getTone(); 
			if(toneTransposer.compareTones(playedTone, expectedTone)){
				HostileHarmonics.printWithTimestamp(playerConfig.getPlayerName() + ": Matched first entity!");
				removeFirstTone();
				// TODO: Instead of just calling score event, instead pass to a score keeper
				playerConfig.getBookKeeper().correctActionPerformed(currentMode.getDifficulty());
			} else {
				HostileHarmonics.printWithTimestamp("Did not match first entity!");
				// TODO: Instead of just calling score event, instead pass to a score keeper
				playerConfig.getBookKeeper().wrongActionPerformed();
			}
			numProcessed++;
		}
		if(currToneList.size() != numProcessed){
			System.out.println(playerConfig.getPlayerName() + ": Warning! Tones Remaining: " + 
								currToneList.subList(numProcessed, currToneList.size()));
		}
		currToneList.clear();
		if(chordEventHolder.get() != null){
			validateAndChangeChord(chordEventHolder.get().getPlayedChord().getChord());
		}
		
		try {
			addMoreNotesIfRequired();
		} catch (SlickException e) {
			HostileHarmonics.printWithTimestamp(playerConfig.getPlayerName() + 
												": Could not add more notes, something bad happened!");
			e.printStackTrace();
		}
		
	}
	
	private void validateAndChangeChord(Chord chord){
		//TODO: This will breakdown if powerups use the same chords
		if(chord.equals(currentChord)){
			// No change required
			return;
		}
		TonePlayMode newTonePlayMode = playerConfig.getTonePlayModeFor(chord);
		if(newTonePlayMode == null){
			// Not interested in this chord
			return;
		}
		selectChord(chord, newTonePlayMode);
	}
	
	private void selectChord(Chord chord, TonePlayMode tonePlayMode){
		Chord prevChord = currentChord;
		
		if(prevChord != null){
			Entity prevIndicator = chordIndicators.get(prevChord);
			// Inefficient, but happens rarely
			Position position = prevIndicator.getComponent(Position.class);
			position.setPosition(position.getXPos(), position.getYPos()+CHORD_INDICATOR_RAISE_HEIGHT);
			prevIndicator.changedInWorld();
		}
		
		Entity newIndicator = chordIndicators.get(chord);
		// Inefficient, but happens rarely
		Position newPosition = newIndicator.getComponent(Position.class);
		newPosition.setPosition(newPosition.getXPos(), newPosition.getYPos()-CHORD_INDICATOR_RAISE_HEIGHT);
		newIndicator.changedInWorld();
		
		currentChord = chord;
		currentMode = tonePlayMode;
		playerConfig.getBookKeeper().setActive(currentMode.getActiveBook());
		
		for(Entity currNote : entityQueue){
			currNote.deleteFromWorld();
		}
		entityQueue.clear();
		latestEntity = null;
	}
	
	private void clearPlayedToneQueue(){
		playedToneQueue[0].clear();
		playedToneQueue[1].clear();
		// I wonder if both should be cleared
	}

	@Override
	protected boolean checkProcessing() {
		return true;
	}
	
	// These need adapters to also be used to also get simulated calls (for clearing out items, etc)
	@Override
	public void tonePlayed(ToneEvent te) {
		if(!gameState.isPaused()){
			playedToneQueue[toneQIdx.get()].add(te);
		} else {
			HostileHarmonics.printWithTimestamp("Ignoring tone cause game is paused.");
		}
	}
	
	@Override
	public void chordPlayed(ChordEvent ce) {
		chordEventHolder.set(ce);
	}
	
	//TODO: All of these are still untested.

	@Override
	public void init(GameState gameState) {
		this.gameState = gameState;
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
	}

	@Override
	public void pause() {
		clearPlayedToneQueue();
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void reset() {
		clearPlayedToneQueue();
	}
	
	@Override
	public void end() {
		clearPlayedToneQueue();
	}
}
