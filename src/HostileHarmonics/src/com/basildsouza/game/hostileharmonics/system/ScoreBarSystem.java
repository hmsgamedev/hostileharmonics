package com.basildsouza.game.hostileharmonics.system;

import org.newdawn.slick.Image;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.utils.ImmutableBag;
import com.basildsouza.game.hostileharmonics.component.Sprite;
import com.basildsouza.game.hostileharmonics.manager.BookKeeper.Book;

public class ScoreBarSystem extends EntitySystem{
	private final Image[] scoreDisplayArr;
	private final Sprite[] spriteComponents;
	private final Book p1Book;
	private final Book p2Book;
	
	private int percentage = 50;
	
	
	public ScoreBarSystem(Book p1Book, Book p2Book, Image[] scoreDisplayArr, Sprite[] spriteComponents) {
		// Not interested in anything since we get the entities externally
		super(Aspect.getEmpty()); 
		
		this.scoreDisplayArr = scoreDisplayArr;
		this.spriteComponents = spriteComponents;
		if(spriteComponents.length != 10){
			throw new IllegalStateException("Currently only size 10 arrays are supported");
		}
		
		this.p1Book = p1Book;
		this.p2Book = p2Book;
	}
	
	@Override
	protected void processEntities(ImmutableBag<Entity> entities) {
		final double denominatorSum = (p1Book.getResourceCount() + 
		 		 					p2Book.getResourceCount());
		if(denominatorSum <= 0.1){
			// We dont want to change anything till we dont have a percentage
			return;
		}
		final int newPerc = (int) Math.round(100.00 * p1Book.getResourceCount() / denominatorSum);
		if(newPerc == this.percentage){
			// No change in perc
			return;
		}
		this.percentage = newPerc;
//		System.out.println("Percentage: " + newPerc + 
//						   " Score1: " + p1Book.getResourceCount() + 
//						   " Score 2: " + p2Book.getResourceCount());
						 		 
		for(int i=0; i<spriteComponents.length; i++){
			int imageIndex = Math.min(Math.max(0,  newPerc - i*10), scoreDisplayArr.length-1);
//			System.out.println(i + " : " + imageIndex);
			spriteComponents[i].setImage(scoreDisplayArr[imageIndex]);
		}
	}
	
	@Override
	protected boolean checkProcessing() {
		return true;
	}
}
