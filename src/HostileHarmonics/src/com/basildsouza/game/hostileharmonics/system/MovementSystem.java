package com.basildsouza.game.hostileharmonics.system;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.basildsouza.game.hostileharmonics.component.Position;
import com.basildsouza.game.hostileharmonics.component.Velocity;

public class MovementSystem extends EntityProcessingSystem{
	@Mapper
	ComponentMapper<Position> positionMapper;
	
	@Mapper
	ComponentMapper<Velocity> velocityMapper;

	@SuppressWarnings("unchecked")
	public MovementSystem() {
		super(Aspect.getAspectForAll(Position.class, Velocity.class));
	}

	@Override
	protected void process(Entity e) {
		final Position position = positionMapper.get(e);
		final Velocity velocity = velocityMapper.get(e);
		final float delta = (float) (world.getDelta() / 1000.0);
		position.addPos(velocity.getVx() * delta, velocity.getVy() * delta);
	}

}
