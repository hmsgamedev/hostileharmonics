package com.basildsouza.game.hostileharmonics.system;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.basildsouza.game.hostileharmonics.component.Position;
import com.basildsouza.game.hostileharmonics.component.Destination;
import com.basildsouza.game.hostileharmonics.component.Velocity;

public class MoveToDestinationSystem extends EntityProcessingSystem{
	
	@Mapper
	ComponentMapper<Position> positionMapper;
	
	@Mapper
	ComponentMapper<Destination> destinationMapper;
	
	@Mapper
	ComponentMapper<Velocity> velocityMapper;
	
	@SuppressWarnings("unchecked")
	public MoveToDestinationSystem() {
		super(Aspect.getAspectForAll(Destination.class, Position.class, Velocity.class));
	}

	@Override
	protected void process(Entity e) {
		final Position position = positionMapper.get(e);
		final Destination destination = destinationMapper.get(e);
		final Velocity velocity = velocityMapper.get(e);
		
		// Replace tolerance distance with x and y tolerance
		if(position.getPosition().distanceSq(destination.getTargetPos()) 
				<= destination.getToleranceSq()){
			velocity.setVelocity(0, 0);
			if(destination.isSnapToDestination()){
				position.setPosition(destination.getTargetPos().x, 
									 destination.getTargetPos().y);
			}
			
			// We remove the destination  
			e.removeComponent(Destination.class);
			e.changedInWorld();
			return;
		}
		float velX;
		float velY;
		
		
		//TODO: (VLow) Later convert this to a function and also a speed curve
		if(position.getXPos() < destination.getTargetPos().getX()){
			velX = destination.getMaxVelocity().getX();
		} else if(position.getXPos() == destination.getTargetPos().getX()){
			velX = 0;
		} else {
			velX = -1 * destination.getMaxVelocity().getX();
		}
		
		if(position.getYPos() < destination.getTargetPos().getY()){
			velY = destination.getMaxVelocity().getY();
		} else if (position.getYPos() == destination.getTargetPos().getY()) {
			velY = 0;
		} else {
			velY = -1 * destination.getMaxVelocity().getY();
		}
		
		velocity.setVelocity(velX, velY);
	}
	
	

}
