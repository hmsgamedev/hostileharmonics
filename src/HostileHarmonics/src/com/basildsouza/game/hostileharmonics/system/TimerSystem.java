package com.basildsouza.game.hostileharmonics.system;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.Timer;
import com.basildsouza.game.hostileharmonics.component.Timed;

public class TimerSystem extends EntityProcessingSystem {
	@Mapper
	private ComponentMapper<Timed> timerMapper;

	
	@SuppressWarnings("unchecked")
	public TimerSystem() {
		super(Aspect.getAspectForAll(Timed.class));
	}

	@Override
	protected void process(Entity e) {
		final Timed timed = timerMapper.get(e);
		
		final Timer timer = timed.getTimer();
		timer.update(world.getDelta());
		
		
		if(timer.isDone()){
			e.removeComponent(Timed.class);
			e.changedInWorld();
		}
	}

}
