package com.basildsouza.game.hostileharmonics.system;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.basildsouza.game.hostileharmonics.component.Position;
import com.basildsouza.game.hostileharmonics.component.Sprite;
import com.basildsouza.game.hostileharmonics.utils.DrawLayerManager;

/**
 * This should NOT be added as a active member to the world (set passive to
 * true)
 * 
 * @author Basil Dsouza
 * 
 */
public class RenderSystem extends EntityProcessingSystem {
	//http://code.google.com/p/gamadu-starwarrior/source/browse/src/com/gamadu/starwarrior/systems/RenderSystem.java
	private GameContainer container;

	@Mapper
	private ComponentMapper<Sprite> spriteMapper;

	@Mapper
	private ComponentMapper<Position> positionMapper;
	private DrawLayerManager drawLayerManager;

	@SuppressWarnings("unchecked")
	public RenderSystem(DrawLayerManager layerManager) {
		super(Aspect.getAspectForAll(Position.class, Sprite.class));
		this.drawLayerManager = layerManager;
	}

	public void render(GameContainer gameContainer, Graphics graphics, 
					   int xOffset, int yOffset){
		container = gameContainer;
		//TODO: Later incorporate cameras and all
		graphics.setClip(0, 0, container.getWidth(), container.getHeight());
		graphics.setBackground(Color.white);
		// This will ensure all actives are added to the sprite layers		
		drawLayerManager.clear();
		process();
		drawLayerManager.renderLayers(graphics, xOffset, yOffset);
		//drawStatistics(graphics);
	}
	
	@Override
	protected void process(Entity e) {
		final Sprite sprite = spriteMapper.get(e);
		final Position position = positionMapper.get(e);
		
		if (sprite != null && position != null) {
			drawLayerManager.addSprite(position, sprite);
		}
	}

	protected void drawStatistics(Graphics graphics) {
		graphics.setColor(Color.black);
		graphics.drawString("Active entities: "
				+ world.getEntityManager().getActiveEntityCount(), 10, 25);
		graphics.drawString("Total added to world since start: "
				+ world.getEntityManager().getTotalAdded(), 10, 40);
		graphics.drawString("Total created in world since start: "
				+ world.getEntityManager().getTotalCreated(), 10, 55);
		graphics.drawString("Total deleted from world since start: "
				+ world.getEntityManager().getTotalDeleted(), 10, 70);
	}
}
