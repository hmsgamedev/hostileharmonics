package com.basildsouza.game.hostileharmonics.component;

import com.artemis.Component;
import com.artemis.utils.Timer;

public class Timed extends Component{
	private Timer timer;
	
	public Timed(Timer timer) {
		this.timer = timer;
	}
	
	public Timer getTimer() {
		return timer;
	}
	
	public void setTimer(Timer timer) {
		this.timer = timer;
	}
}
