package com.basildsouza.game.hostileharmonics.component;

import java.awt.geom.Point2D;

import org.newdawn.slick.geom.Vector2f;

import com.artemis.Component;

public class Destination extends Component{
	private Point2D.Float targetPos;
	private Vector2f maxVelocity;
	private int toleranceDistance;
	private int toleranceSq;
	private boolean snapToDestination;
	
	public Destination(Point2D.Float targetPos, Vector2f maxVelocity, int toleranceDistance, boolean snapToDest) {
		setTargetPos(targetPos);
		setMaxVelocity(maxVelocity);
		setToleranceDistance(toleranceDistance);
		setSnapToDestination(snapToDest);
	}
	
	public Destination(Point2D.Float targetPos, Vector2f maxVelocity, int toleranceDistance) {
		this(targetPos, maxVelocity, toleranceDistance, true);
	}
	
	public boolean isSnapToDestination() {
		return snapToDestination;
	}
	
	public void setSnapToDestination(boolean snapToDestination) {
		this.snapToDestination = snapToDestination;
	}
	
	public Vector2f getMaxVelocity() {
		return maxVelocity;
	}
	
	public void setMaxVelocity(Vector2f maxVelocity) {
		this.maxVelocity = maxVelocity;
	}
	
	public Point2D.Float getTargetPos() {
		return targetPos;
	}
	
	public void setTargetPos(Point2D.Float targetPos) {
		this.targetPos = targetPos;
	}
	
	public int getToleranceDistance() {
		return toleranceDistance;
	}
	
	public void setToleranceDistance(int toleranceDistance) {
		this.toleranceDistance = toleranceDistance;
		this.toleranceSq = toleranceDistance * toleranceDistance;
	}
	
	public int getToleranceSq() {
		return toleranceSq;
	}
}
