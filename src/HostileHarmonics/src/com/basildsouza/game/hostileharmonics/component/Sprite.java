package com.basildsouza.game.hostileharmonics.component;

import java.awt.geom.Point2D;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import com.artemis.Component;
import com.basildsouza.game.hostileharmonics.factory.SpriteFactory.Layer;

public class Sprite extends Component{
	private Image sprite;
	private Layer layer;
	private Point2D.Float internalOffset;
	//Note: Can put other transforms here
	
	public Sprite(Image sprite, Layer layer) {
		this.sprite = sprite;
		this.layer = layer;
	}
	
	public Sprite(Image sprite, Layer layer, Point2D.Float internalOffset) {
		this.sprite = sprite;
		this.layer = layer;
		this.internalOffset = internalOffset;
	}
	
	public Point2D.Float getInternalOffset() {
		return internalOffset;
	}
	
	public void setInternalOffset(Point2D.Float internalOffset) {
		this.internalOffset = internalOffset;
	}
	
	public Image getImage() {
		return sprite;
	}
	
	public void setImage(Image sprite) {
		this.sprite = sprite;
	}
	
	public Layer getLayer() {
		return layer;
	}
	
	public void setLayer(Layer layer) {
		this.layer = layer;
	}
	
	public void render(Graphics g, float x, float y){
		//TODO: Delegate rendering to something else? also, perhaps render based on centre coord instead of "offset"
		Point2D.Float internalOffset = getInternalOffset();
		if(internalOffset != null){
			g.drawImage(getImage(), x+internalOffset.x, y+internalOffset.y);
		} else {
			g.drawImage(getImage(), x, y);
		}
	}
}
