package com.basildsouza.game.hostileharmonics.component;

import java.awt.geom.Point2D;

import com.artemis.Component;

public class Position extends Component {
	private Point2D.Float position;
	private float rotation;

	public Position(float xPos, float yPos) {
		this(xPos, yPos, 0);
	}

	public Position(float xPos, float yPos, float rotation) {
		this(new Point2D.Float(xPos, yPos), rotation);
	}
	
	public Position(Point2D.Float position, float rotation) {
		this.position = position;
		this.rotation = rotation;
	}

	public float getRotation() {
		return rotation;
	}

	public float getXPos() {
		return position.x;
	}

	public float getYPos() {
		return position.y;
	}
	
	public Point2D.Float getPosition() {
		return position;
	}
	
	public void addPos(float x, float y){
		this.position.x += x;
		this.position.y += y;
	}

/*	public void addXPos(float x) {
		position.x += x;
	}

	public void addYPos(float y) {
		this.position.y += y;
	}
*/
	public void addRotation(float angle) {
		rotation = (rotation + angle) % 360;
	}

	public float getRotationAsRadians() {
		return (float) Math.toRadians(rotation);
	}

	public float getDistanceSqTo(Position t) {
		return (float) position.distanceSq(t.getPosition());
	}
	
	public float getDistanceTo(Position t) {
		return (float) position.distance(t.getPosition());
	}
	
	public void setPosition(float x, float y){
		position.setLocation(x, y);
	}
}