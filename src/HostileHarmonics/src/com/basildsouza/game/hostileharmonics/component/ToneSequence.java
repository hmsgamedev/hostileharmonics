package com.basildsouza.game.hostileharmonics.component;

import com.artemis.Component;
import com.basildsouza.tools.music.piano.engine.Clef;
import com.basildsouza.tools.music.piano.engine.Tone;

public class ToneSequence extends Component {
	private final Tone tone;
	private final Clef clef;
	
	public ToneSequence(Clef clef, Tone tone) {
		this.clef = clef;
		this.tone = tone;
	}
	
	public Tone getTone() {
		return tone;
	}
	
	public Clef getClef() {
		return clef;
	}
}



/*
 * Previous design, before i used entity lists
 
public class ToneSequence extends Component {
private final Tone tone;
private final ToneSequenceHolder firstAndLastTone;
private int index;
private ToneSequence nextTone;

public ToneSequence(Tone tone, ToneSequenceHolder firstTone) {
	this.tone = tone;
	this.firstAndLastTone = firstTone;
}

public ToneSequence getFirstTone() {
	return firstAndLastTone.getFirst();
}

public int getIndex() {
	return index;
}

public void moveAhead() {
	this.index--;
}

public Tone getTone() {
	return tone;
}

public ToneSequence getNextTone() {
	return nextTone;
}

public void setNextTone(ToneSequence nextTone) {
	this.nextTone = nextTone;
}

public static class ToneSequenceHolder {
	private ToneSequence first;
	private ToneSequence last;
	
	public ToneSequenceHolder(ToneSequence first) {
		this(first, first);
	}
	
	public ToneSequenceHolder(ToneSequence first, ToneSequence last) {
		this.first = first;
		this.last = last;
	}
	
	public void setFirst(ToneSequence first) {
		this.first = first;
	}
	
	public ToneSequence getFirst() {
		return first;
	}
	
	public ToneSequence getLast() {
		return last;
	}
	
	public void setLast(ToneSequence last) {
		this.last = last;
	}
}
}
*/