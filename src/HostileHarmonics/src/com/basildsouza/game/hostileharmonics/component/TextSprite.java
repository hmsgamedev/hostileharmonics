package com.basildsouza.game.hostileharmonics.component;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;

import com.basildsouza.game.hostileharmonics.factory.SpriteFactory.Layer;

public class TextSprite extends Sprite {
	
	private final TextProvider textProvider;

	public TextSprite(Layer layer, 
					  final String text, final Font font) {
		this(layer, new TextProvider() {
			
			@Override
			public String getText() {
				return text;
			}
			
			@Override
			public Font getFont() {
				return font;
			}
		});
	}
	
	public TextSprite(Layer layer, TextProvider textProvider) {
		super(null, layer);
		this.textProvider = textProvider;
	}
	
	@Override
	public void render(Graphics g, float x, float y) {
		if(getImage() != null){
			super.render(g, x, y);
		}
		final Font font = g.getFont();
		Color color = g.getColor();
		g.setColor(Color.black);
		
		g.setFont(textProvider.getFont());
		g.drawString(textProvider.getText(), x, y);
		
		g.setFont(font);
		g.setColor(color);
	}
	
	public static interface TextProvider{
		public String getText();
		public Font getFont();
	}

}
