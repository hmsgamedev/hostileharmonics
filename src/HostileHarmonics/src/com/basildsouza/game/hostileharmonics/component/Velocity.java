package com.basildsouza.game.hostileharmonics.component;

import org.newdawn.slick.geom.Vector2f;

import com.artemis.Component;

public class Velocity extends Component {
	private Vector2f velocity;

	public Velocity() {
		this(0, 0);
	}

	public Velocity(float vx, float vy) {
		this(new Vector2f(vx, vy));
	}
	
	public Velocity(Vector2f velocity) {
		this.velocity = velocity;
	}

	public float getVx() {
		return velocity.getX();
	}

	public void setVx(float vx) {
		this.velocity.set(vx, getVy());
	}

	public float getVy() {
		return velocity.getY();
	}

	public void setVy(float vy) {
		this.velocity.set(getVx(), vy);
	}
	
	public void setVelocity(float x, float y) {
		this.velocity.set(x, y);
	}
}