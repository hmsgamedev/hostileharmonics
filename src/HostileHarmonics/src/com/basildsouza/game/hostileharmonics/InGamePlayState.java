package com.basildsouza.game.hostileharmonics;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.Map;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.PlayerManager;
import com.basildsouza.game.hostileharmonics.factory.HUDEntityFactory;
import com.basildsouza.game.hostileharmonics.factory.MusicEntityFactory;
import com.basildsouza.game.hostileharmonics.factory.SpriteFactory.Layer;
import com.basildsouza.game.hostileharmonics.manager.BookKeeper.Book;
import com.basildsouza.game.hostileharmonics.states.GameStateAware;
import com.basildsouza.game.hostileharmonics.states.InGameState;
import com.basildsouza.game.hostileharmonics.system.MoveToDestinationSystem;
import com.basildsouza.game.hostileharmonics.system.MovementSystem;
import com.basildsouza.game.hostileharmonics.system.NoteGeneratorSystem;
import com.basildsouza.game.hostileharmonics.system.RenderSystem;
import com.basildsouza.game.hostileharmonics.system.TimerSystem;
import com.basildsouza.game.hostileharmonics.utils.DrawLayerManager;
import com.basildsouza.tools.music.piano.engine.Clef;
import com.basildsouza.tools.music.piano.engine.chord.Chord;
import com.basildsouza.tools.music.piano.engine.event.PlayerDevice;

public class InGamePlayState extends BasicGameState{
	public final static int GAME_STATE_ID = GameStateIDContainer.IN_GAME_STATE;

	private World world;
	private RenderSystem renderSystem;

	private final LoadingState loadingState;

	public InGamePlayState(LoadingState loadingState) {
		this.loadingState = loadingState;
	}


	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {

		//TODO: Perhaps in the future this also should be moved to loading
		world = new World();
		world.setManager(new PlayerManager());
		world.setSystem(new MoveToDestinationSystem());
		world.setSystem(new MovementSystem());
		world.setSystem(new TimerSystem());
		
		renderSystem = world.setSystem(new RenderSystem(new DrawLayerManager(Layer.values().length)), true);
		
		container.setShowFPS(true);
		container.setMinimumLogicUpdateInterval(100);
		//container.setMaximumLogicUpdateInterval(200);
		//container.setTargetFrameRate(100);
		
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game)
			throws SlickException {
		//TODO: Currently this wil only enter once (since there is no pause state)
		// Later need to add better logic on when to add listeners and stuff
		HostileHarmonics.printWithTimestamp("Entered InGamePlayState");
		container.pause();

		MusicEntityFactory musicEntityFactory = loadingState.getMusicEntityFactory();
		HUDEntityFactory hudEntityFactory = loadingState.getHudEntityFactory();
		
		//TODO: This should eventually be at class level or even singleton?
		InGameState theGameState = new InGameState(container);

		HostileHarmonics.printWithTimestamp("Initialized sprites");
		GameConfig gameConfig = GameConfig.getInstance();
		int numPlayers = gameConfig.getNumPlayers();
		final Book mainBooks[] = new Book[2];
		for(int i=0; i<numPlayers; i++){
			PlayerConfig playerConfig = gameConfig.getConfigForPlayer(i);
			
			final int startPos = (int) (playerConfig.getBarHelper()
					   .getBarPosition(Clef.Treble).x);
			
			Map<Chord, Entity> chordIndicatorMap = hudEntityFactory.createChordIndicators(world, playerConfig.getTonePlayEntries(), new Point2D.Float(startPos+10, 300));

			NoteGeneratorSystem noteGeneratorSystem 
					= new NoteGeneratorSystem(playerConfig, musicEntityFactory, chordIndicatorMap);
			theGameState.register(noteGeneratorSystem);
			
			PlayerDevice playerDevice = playerConfig.getPlayerDevice();
			playerDevice.addToneListener(noteGeneratorSystem);
			playerDevice.addChordListener(noteGeneratorSystem);
			//TODO: Later add other chord listeners as well, perhaps in other places
			
			world.setSystem(noteGeneratorSystem);
			
			final List<Book> allBooks = playerConfig.getBookKeeper().getAllBooks();
			hudEntityFactory.createScoreEntities(world, allBooks, 
												 startPos, 200);
			
			hudEntityFactory.createMultiplierEntity(world, playerConfig.getBookKeeper(), 
													startPos, 200).addToWorld();
			
			mainBooks[i] = playerConfig.getScoreBook();
			
		}
		
		world.setSystem(hudEntityFactory.createMainScoreBarSystem(world, mainBooks[0], mainBooks[1], 
												 Color.blue, Color.red, new Point2D.Float(100, 100), 600, game));
		
		world.initialize();
		
		theGameState.init();
		theGameState.start();
		container.resume();
		
		HostileHarmonics.printWithTimestamp("Completed InGamePlayState state enter");
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		renderSystem.render(container, g, 0, 0);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		world.setDelta(delta);
		world.process();
		
	}

	@Override
	public int getID() {
		return GAME_STATE_ID;
	}

}
