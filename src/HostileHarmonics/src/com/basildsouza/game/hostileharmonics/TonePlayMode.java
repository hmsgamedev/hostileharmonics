package com.basildsouza.game.hostileharmonics;

import com.basildsouza.game.hostileharmonics.manager.BookKeeper.Book;
import com.basildsouza.tools.music.piano.engine.generator.TuneGenerator;

public class TonePlayMode implements Comparable<TonePlayMode>{
	private final String name;
	private final double difficulty;
	private final Book activeBook;
	private final TuneGenerator toneSequenceGenerator;
	private final int order;
	
	public TonePlayMode(String name,
						double difficulty, 
						TuneGenerator toneSequenceGenerator,
						int order,
						Book activeBook) {
		this.name = name;
		this.difficulty = difficulty;
		this.order = order;
		this.activeBook = activeBook;
		this.toneSequenceGenerator = toneSequenceGenerator;
	}
	
	public String getName() {
		return name;
	}
	
	public Book getActiveBook() {
		return activeBook;
	}
	
	public double getDifficulty() {
		return difficulty;
	}
	
	public TuneGenerator getToneSequenceGenerator() {
		return toneSequenceGenerator;
	}

	@Override
	public int compareTo(TonePlayMode o) {
		return order-o.order;
	}
}
