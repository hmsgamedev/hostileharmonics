package com.basildsouza.game.hostileharmonics;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * 
 * @author Basil Dsouza
 *
 */
public class HostileHarmonics extends StateBasedGame {
/*TODO: Need to use java 7 to see the Midi Yoke
 * 
 */
	public HostileHarmonics() {
		super("Hostile Harmonics");
	}
	
	@Override
	public void initStatesList(GameContainer container) throws SlickException {
		addState(new TempMidiSelectorState());
		LoadingState loadingState = new LoadingState();
		addState(new InGamePlayState(loadingState));
		addState(loadingState);
		addState(new TempEndGameState());
	}

	public static void main(String[] args) throws SlickException {
		System.out.println(System.getProperty("java.home"));
		 HostileHarmonics game = new HostileHarmonics();
		 GameStateManager.createInstance(game);
         AppGameContainer container = new AppGameContainer(game);
         container.setDisplayMode(800, 600, false);
         container.setAlwaysRender(true);
         // container.setMinimumLogicUpdateInterval(1);
         // container.setMaximumLogicUpdateInterval(1);
         // container.setTargetFrameRate(60);
         container.start();
	}
	
	private static final DateFormat FORMATTER = new SimpleDateFormat("HH:mm:ss");
	public static void printWithTimestamp(String message){
		System.out.println(FORMATTER.format(new Date(System.currentTimeMillis())) + ": " + message);
	}
	
}
