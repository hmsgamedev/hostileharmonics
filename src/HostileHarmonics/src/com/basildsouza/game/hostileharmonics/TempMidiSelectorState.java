package com.basildsouza.game.hostileharmonics;

import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiDevice.Info;
import javax.sound.midi.MidiUnavailableException;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import com.basildsouza.game.hostileharmonics.manager.BookKeeper;
import com.basildsouza.game.hostileharmonics.manager.BookKeeper.Book;
import com.basildsouza.game.hostileharmonics.utils.BarHelper;
import com.basildsouza.game.hostileharmonics.utils.Pair;
import com.basildsouza.tools.music.piano.engine.FixedOffsetToneChecker;
import com.basildsouza.tools.music.piano.engine.MidiDeviceLister;
import com.basildsouza.tools.music.piano.engine.MidiEngine;
import com.basildsouza.tools.music.piano.engine.Note;
import com.basildsouza.tools.music.piano.engine.NoteLetter;
import com.basildsouza.tools.music.piano.engine.PlayerMidiKeyListener;
import com.basildsouza.tools.music.piano.engine.Tone;
import com.basildsouza.tools.music.piano.engine.chord.Chord;
import com.basildsouza.tools.music.piano.engine.chord.ChordBank;
import com.basildsouza.tools.music.piano.engine.chord.ChordDetector;
import com.basildsouza.tools.music.piano.engine.generator.RandomTuneGenerator;

public class TempMidiSelectorState extends BasicGameState implements KeyListener{
	public final static int GAME_STATE_ID = GameStateIDContainer.TEMP_MIDI_SELECTOR;

	private volatile int deviceSelected = -1;
	private MidiDeviceLister devicelister;
	
	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		devicelister = MidiDeviceLister.getInstance();
		refreshDevices();
		container.setTargetFrameRate(60);
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game)
			throws SlickException {
		refreshDevices();
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		g.drawString("Listing Midi Input Devices. Press the corresponding number to use device", 10, 60);
		g.drawString("Press F5 to refresh list", 10, 80);
		final List<MidiDevice> inpDevices = devicelister.getMidiInputDevices();
		int deviceNum = 1;
		for(MidiDevice device : inpDevices){
			Info deviceInfo = device.getDeviceInfo();
			g.drawString("Device: " + deviceNum + " - " + deviceInfo.getName(), 10, 100+deviceNum*20);
			deviceNum++;
		}
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		final Input input = container.getInput();
		if(deviceSelected != -1){
			final int deviceNum = deviceSelected;
			List<MidiDevice> devices = devicelister.getMidiInputDevices();
			if(deviceNum >= devices.size()){
				//TODO: Perhaps show an indication?
				return;
			}
			try {
				deviceSelected(devices.get(deviceNum));
			} catch (MidiUnavailableException e) {
				e.printStackTrace();
				//TODO: Eventually just show error on screen and allow it to continue
				throw new SlickException("Unable to create connection to selected midi device", e);
			}
			
			//game.enterState(InGamePlayState.GAME_STATE_ID);
			game.enterState(LoadingState.GAME_STATE_ID, new FadeOutTransition(), new FadeInTransition());
			return;
		}
		if(input.isKeyPressed(Input.KEY_F5)){
			System.out.println("F5 Pressed Refreshing midi device list.");
			refreshDevices();
		}
	}
	
	private void deviceSelected(MidiDevice midiDevice) throws MidiUnavailableException{
		HostileHarmonics.printWithTimestamp("Device Selected: " + midiDevice.getDeviceInfo().getName());
		//TODO: Midi engine also should be done in some other way later
		MidiEngine midiEngine = new MidiEngine(midiDevice);
		
		ChordBank chordBank = new ChordBank();
		ChordDetector chordDetector = new ChordDetector(chordBank);
		
		Tone p1TMC = new Tone(new Note(NoteLetter.C), 2);
		Tone p1BMC = new Tone(new Note(NoteLetter.C), 4);
		Tone p2TMC = new Tone(new Note(NoteLetter.C), 5);
		Tone p2BMC = new Tone(new Note(NoteLetter.C), 7);
		
		createPlayerConfig("Player 1", 0, chordDetector, p1BMC, p1TMC, midiEngine);
		createPlayerConfig("Player 2", 400, chordDetector, p2BMC, p2TMC, midiEngine);
		
		HostileHarmonics.printWithTimestamp("Player Configs created!");
	}
	
	//TODO: This should eventually go somewhere else or be created in parts
	@SuppressWarnings("unchecked")
	private void createPlayerConfig(String player, int xPos, ChordDetector chordDetector, 
									Tone bassMidC, Tone trebleMidC, MidiEngine midiEngine){
		
		final BarHelper barHelper 
					= new BarHelper(new Point2D.Float(xPos, 450), 
				  					new Point2D.Float(xPos, 525), 
				  					player, 19, 20, 20);

		final PlayerMidiKeyListener playerListener 
						= new PlayerMidiKeyListener(chordDetector, bassMidC, trebleMidC);
		midiEngine.addMidiKeyListener(playerListener);
		
		final PlayerConfig playerConf 
			= new PlayerConfig(player, barHelper, playerListener, 
							   RandomTuneGenerator.GRAND_CENTRAL_NOTES, 
							   new FixedOffsetToneChecker(trebleMidC, bassMidC));
		
		final BookKeeper bookKeeper = new BookKeeper();
		// TODO: Later give this fancier names, like difficulty, etc
		final Book tScoreBook = bookKeeper.createBook("Treble");
		final Book bScoreBook = bookKeeper.createBook("Bass");
		final Book gScoreBook = bookKeeper.createBook("Grand");

		final Book scoreBook 
				= bookKeeper.createMetaBook("Score", 
								  new Pair[]{
										new Pair<Book, Double>(tScoreBook, 1.0),
										new Pair<Book, Double>(bScoreBook, 1.0),
										new Pair<Book, Double>(gScoreBook, 1.0)
									});
		playerConf.setScoreBook(scoreBook);
		
		final TonePlayMode treblePlayMode 
						= new TonePlayMode("Treble", 1, RandomTuneGenerator.TREBLE_CENTRAL_NOTES, 1, tScoreBook);
		final TonePlayMode trebleAccPlayMode 
						= new TonePlayMode("Treble #/b", 2, RandomTuneGenerator.TREBLE_CENTRAL_NOTES_WITH_ACCIDENTALS, 2, tScoreBook);
		final TonePlayMode bassPlayMode 
						= new TonePlayMode("Bass", 1, RandomTuneGenerator.BASS_CENTRAL_NOTES, 3, bScoreBook);
		final TonePlayMode bassAccPlayMode 
						= new TonePlayMode("Bass #/b", 2, RandomTuneGenerator.BASS_CENTRAL_NOTES_WITH_ACCIDENTALS, 4, bScoreBook);
		final TonePlayMode grandPlayMode 
						= new TonePlayMode("Grand", 1, RandomTuneGenerator.GRAND_CENTRAL_NOTES, 5, gScoreBook);
		final TonePlayMode grandAccPlayMode 
						= new TonePlayMode("Grand #/b", 2, RandomTuneGenerator.GRAND_CENTRAL_NOTES_WITH_ACCIDENTALS, 6, gScoreBook);
		
		playerConf.setBookKeeper(bookKeeper);
		final Map<Chord, TonePlayMode> tonePlayMap = new HashMap<Chord, TonePlayMode>();
		tonePlayMap.put(ChordBank.C_MAJ, treblePlayMode);
		tonePlayMap.put(ChordBank.C_MIN, trebleAccPlayMode);
		
		tonePlayMap.put(ChordBank.D_MAJ, bassPlayMode);
		tonePlayMap.put(ChordBank.D_MIN, bassAccPlayMode);
		
		tonePlayMap.put(ChordBank.E_MAJ, grandPlayMode);
		tonePlayMap.put(ChordBank.E_MIN, grandAccPlayMode);
		
		playerConf.setDefaultChord(ChordBank.C_MAJ);
		playerConf.setTonePlayMap(tonePlayMap);
		
		GameConfig.getInstance().addPlayer(playerConf);
	}

	@Override
	public void keyPressed(int key, char c) {
		deviceSelected = key - Input.KEY_1;
		System.out.println("Pressed: " + (key - Input.KEY_1));
	}

	@Override
	public int getID() {
		return GAME_STATE_ID;
	}
	
	private void refreshDevices(){
		devicelister.refresh();
	}

}
