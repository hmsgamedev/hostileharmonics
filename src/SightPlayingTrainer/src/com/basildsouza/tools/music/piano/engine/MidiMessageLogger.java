package com.basildsouza.tools.music.piano.engine;

public abstract class MidiMessageLogger {
	private static final char hexDigits[] = 
		{ '0', '1', '2', '3', '4', '5', '6', '7',
	  	 '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	protected static String getHexString(byte[] aByte) {
		StringBuffer sbuf = new StringBuffer(aByte.length * 3 + 2);
		for (int i = 0; i < aByte.length; i++) {
			sbuf.append(' ');
			sbuf.append(hexDigits[(aByte[i] & 0xF0) >> 4]);
			sbuf.append(hexDigits[aByte[i] & 0x0F]);
			/*
			 * byte bhigh = (byte) ((aByte[i] & 0xf0) >> 4); sbuf.append((char)
			 * (bhigh > 9 ? bhigh + 'A' - 10: bhigh + '0')); byte blow = (byte)
			 * (aByte[i] & 0x0f); sbuf.append((char) (blow > 9 ? blow + 'A' -
			 * 10: blow + '0'));
			 */
		}
		return new String(sbuf);
	}

	protected static float convertTempo(float value) {
		if (value <= 0) {
			value = 0.1f;
		}
		return 60000000.0f / value;
	}

}
