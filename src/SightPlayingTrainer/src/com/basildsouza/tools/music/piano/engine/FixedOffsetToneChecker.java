package com.basildsouza.tools.music.piano.engine;

public class FixedOffsetToneChecker implements ToneChecker{
	private final int trebleToneOffset;
	private final int bassToneOffset;
	
	public static final Tone MIDDLE_C = new Tone(new Note(NoteLetter.C), 4);
	
	public FixedOffsetToneChecker(){
		this(MIDDLE_C);
	}
	
	public FixedOffsetToneChecker(final Tone middleC) {
		this(middleC, middleC);
		
	}
	
	public FixedOffsetToneChecker(final Tone trebleMiddleC, final Tone bassMiddleC) {
		trebleToneOffset = trebleMiddleC.getTonicDistance() - MIDDLE_C.getTonicDistance();
		bassToneOffset = bassMiddleC.getTonicDistance() - MIDDLE_C.getTonicDistance();
	}
	
	@Override
	public boolean compareTones(Tone playedTone, Tone expectedTone) {
		return (playedTone.getTonicDistance()-expectedTone.getTonicDistance()) == trebleToneOffset || 
			   (playedTone.getTonicDistance()-expectedTone.getTonicDistance()) == bassToneOffset;
	}

}
