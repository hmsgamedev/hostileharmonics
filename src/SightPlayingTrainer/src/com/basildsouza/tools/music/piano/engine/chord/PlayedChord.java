package com.basildsouza.tools.music.piano.engine.chord;

import java.util.List;

import com.basildsouza.tools.music.piano.engine.Tone;

public class PlayedChord {
	private final Chord chord;
	private final List<Tone> playedNotes;
	private boolean open;

	public PlayedChord(Chord chord, List<Tone> playedNotes, boolean open) {
		this.chord = chord;
		this.playedNotes = playedNotes;
		this.open = open;
	}

	public Chord getChord() {
		return chord;
	}

	public List<Tone> getPlayedNotes() {
		return playedNotes;
	}

	public boolean isOpen() {
		return open;
	}

	@Override
	public String toString() {
		return "PlayedChord ( " + chord + " Played: " + playedNotes + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chord == null) ? 0 : chord.hashCode());
		result = prime * result + (open ? 1231 : 1237);
		result = prime * result
				+ ((playedNotes == null) ? 0 : playedNotes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayedChord other = (PlayedChord) obj;
		if (chord == null) {
			if (other.chord != null)
				return false;
		} else if (!chord.equals(other.chord))
			return false;
		if (open != other.open)
			return false;
		if (playedNotes == null) {
			if (other.playedNotes != null)
				return false;
		} else if (!playedNotes.equals(other.playedNotes))
			return false;
		return true;
	}
}