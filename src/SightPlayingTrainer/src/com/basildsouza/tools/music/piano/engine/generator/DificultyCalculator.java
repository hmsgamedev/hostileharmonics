package com.basildsouza.tools.music.piano.engine.generator;

public interface DificultyCalculator {
	
	
	/* TODO:  
	 * 1. possible implementions are: Simple (for the lack of a better name)
	 * 2. For the simple one:
	 * 		a. Distnace between consecutive keys
	 * 		b. Black to white key ratio, where 0 white have the lowest, all black have about 4/10 and the 0.5 having 10/10
	 * 3. In case of multi clef notes, a switch between clefs iwll also increase difficulty (by how much?)
	 * 
	 */
	public int calculateDifficulty(TransposedTune tune);
}

/**
 * Only reason why there is a difference is cause testing 
 * will be easier if i get around to doing it.
 * 
 * @author Basil Dsouza
 */
class DifficultCalculatorImpl {
	
}