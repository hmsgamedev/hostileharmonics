package com.basildsouza.tools.music.piano.engine.chord;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.basildsouza.tools.music.piano.engine.Note;
import static com.basildsouza.tools.music.piano.engine.NoteLetter.*;
import static com.basildsouza.tools.music.piano.engine.Accidental.*;
/**
 * This class should eventually contain a list of chords 
 * queriable by various criteria. Currently it only 
 * supports get all chords
 *  
 * @author Basil Dsouza
 *
 */
public class ChordBank {
	private final List<Chord> chordList = new LinkedList<Chord>();
	private static final Note CN = new Note(C, Natural);
	private static final Note CS = new Note(C, Sharp);
	private static final Note DN = new Note(D, Natural);
	private static final Note DS = new Note(D, Sharp);
	private static final Note EN = new Note(E, Natural);
	private static final Note FN = new Note(F, Natural);
	private static final Note FS = new Note(F, Sharp);
	private static final Note GN = new Note(G, Natural);
	private static final Note GS = new Note(G, Sharp);
	private static final Note AN = new Note(A, Natural);
	private static final Note AS = new Note(A, Sharp);
	private static final Note BN = new Note(B, Natural);
	
	public static final Chord C_MAJ = new Chord(Arrays.asList(CN, EN, GN), CN, "C Maj");  
	public static final Chord D_MAJ = new Chord(Arrays.asList(DN, FS, AN), DN, "D Maj");
	public static final Chord E_MAJ = new Chord(Arrays.asList(EN, GS, BN), EN, "E Maj");
	public static final Chord F_MAJ = new Chord(Arrays.asList(FN, AN, CN), FN, "F Maj");
	public static final Chord G_MAJ = new Chord(Arrays.asList(GN, BN, DN), GN, "G Maj");
	public static final Chord A_MAJ = new Chord(Arrays.asList(AN, CS, EN), AN, "A Maj");
	public static final Chord B_MAJ = new Chord(Arrays.asList(BN, DS, FS), BN, "B Maj");
	
	public static final Chord C_MIN = new Chord(Arrays.asList(CN, DS, GN), CN, "C Min");
	public static final Chord D_MIN = new Chord(Arrays.asList(DN, FN, AN), DN, "D Min");
	public static final Chord E_MIN = new Chord(Arrays.asList(EN, GN, BN), EN, "E Min");
	public static final Chord F_MIN = new Chord(Arrays.asList(FN, GS, CN), FN, "F Min");
	public static final Chord G_MIN = new Chord(Arrays.asList(GN, AS, DN), GN, "G Min");
	public static final Chord A_MIN = new Chord(Arrays.asList(AN, CN, EN), AN, "A Min");
	public static final Chord B_MIN = new Chord(Arrays.asList(BN, DN, FS), BN, "B Min");

	{
		
		//TODO: This should come from spring eventually
		//Majors:
		chordList.add(C_MAJ);
		chordList.add(new Chord(Arrays.asList(CS, FN, GS), CS, "C# Maj"));
		chordList.add(D_MAJ);
		chordList.add(new Chord(Arrays.asList(DS, GN, AS), DS, "D# Maj"));
		chordList.add(E_MAJ);
		chordList.add(F_MAJ);
		chordList.add(new Chord(Arrays.asList(FS, AS, CS), FS, "F# Maj"));
		chordList.add(G_MAJ);
		chordList.add(new Chord(Arrays.asList(GS, CN, DS), GS, "G# Maj"));
		chordList.add(A_MAJ);
		chordList.add(new Chord(Arrays.asList(AS, DN, FN), AS, "A# Maj"));
		chordList.add(B_MAJ);
		
		//Minors:
		chordList.add(C_MIN);
		chordList.add(new Chord(Arrays.asList(CS, EN, GS), CS, "C# Min"));
		chordList.add(D_MIN);
		chordList.add(new Chord(Arrays.asList(DS, FS, AS), DS, "D# Min"));
		chordList.add(E_MIN);
		chordList.add(F_MIN);
		chordList.add(new Chord(Arrays.asList(FS, AN, CS), FS, "F# Min"));
		chordList.add(G_MIN);
		chordList.add(new Chord(Arrays.asList(GS, BN, DS), GS, "G# Min"));
		chordList.add(A_MIN);
		chordList.add(new Chord(Arrays.asList(AS, CS, FN), AS, "A# Min"));
		chordList.add(B_MIN);
		
		
		
		chordList.add(new Chord(Arrays.asList(CN, CN, EN, GN), CN, "C 5"));
	}
	
	public ChordBank() {
	}
	 
	public List<Chord> getAllRegisteredChords() {
		return chordList;
	}
}
