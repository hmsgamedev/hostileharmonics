package com.basildsouza.tools.music.piano.engine.event;

import com.basildsouza.tools.music.piano.engine.Tone;

public class MidiKeyEvent {
	//TODO: Later add velocity and other stuff here?
	private final Tone tone; 
	private final long time;
	
	public MidiKeyEvent(final Tone tone, final long time) {
		this.tone = tone;
		this.time = time;
	}
	
	public long getTime() {
		return time;
	}
	
	public Tone getTone() {
		return tone;
	}
}
