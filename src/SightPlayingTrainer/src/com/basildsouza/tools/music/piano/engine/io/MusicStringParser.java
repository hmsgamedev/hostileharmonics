package com.basildsouza.tools.music.piano.engine.io;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.basildsouza.tools.music.piano.engine.Accidental;
import com.basildsouza.tools.music.piano.engine.Note;
import com.basildsouza.tools.music.piano.engine.NoteLetter;
import com.basildsouza.tools.music.piano.engine.Tone;

public class MusicStringParser {
	private static final Pattern TONE_PATTERN = Pattern.compile("([ABCDEFG][#b]?[#b]?)?(\\d+)");
	private static final Pattern NOTE_PATTERN = Pattern.compile("([ABCDEFG])([#b]?[#b]?)?");
	
	private static final Map<String, Accidental> accidentalMap 
	= new HashMap<String, Accidental>(){
		private static final long serialVersionUID = 1L;
		{
			for(Accidental acc : Accidental.values()){
				put(acc.toString(), acc);
			}
		}
		
	};
	
	public static Tone parseTone(String toneStr) throws ParseException{
		if(toneStr == null || toneStr.isEmpty()){
			return null;
		}
		Matcher matcher = TONE_PATTERN.matcher(toneStr.trim());
		if(!matcher.matches()){
			throw new ParseException("Unrecognised note: " + toneStr + ". Expected pattern: " + TONE_PATTERN, 0);
		}
		
		int groupCount = matcher.groupCount();
		if(groupCount != 2){
			throw new ParseException("Expected tone to have 2 groups. Expected tone: " + 
								     toneStr + " to match pattern: " + TONE_PATTERN, 0);	
		} else if(groupCount == 2){
			
			return new Tone(parseNote(matcher.group(1)),
							Integer.parseInt(matcher.group(2)));
		}
		return null;
	}
	
	public static Note parseNote(String noteStr) throws ParseException{
		if(noteStr == null){
			return null;
		}
		Matcher matcher = NOTE_PATTERN.matcher(noteStr.trim());
		if(!matcher.matches()){
			throw new ParseException("Unrecognised note: " + noteStr + ". Expected pattern: " + NOTE_PATTERN, 0);
		}
		
		int groupCount = matcher.groupCount();
		if(groupCount == 1){
			return new Note(parseNoteLetter(noteStr));
		} else if(groupCount == 2){
			return new Note(parseNoteLetter(matcher.group(1)), 
							parseAccidental(matcher.group(2)));
		}
		return null;
	}
	
	public static NoteLetter parseNoteLetter(String noteLetterStr){
		return NoteLetter.valueOf(noteLetterStr);
	}
	
	public static Accidental parseAccidental(String accidentalStr){
		if(accidentalStr != null){
			return accidentalMap.get(accidentalStr.trim());
		}
		return null;
	}
	
	
	private static Tone tone(String toneStr){
		Matcher matcher = TONE_PATTERN.matcher(toneStr);
		if(!matcher.matches() || matcher.groupCount() != 3){
			throw new IllegalArgumentException("Invalid String: " + toneStr);
		}
		
		NoteLetter noteLetter = NoteLetter.valueOf(matcher.group(1));
		int octave = Integer.parseInt(matcher.group(3));
		
		String accString = matcher.group(2);
		if("#".equals(accString)){
			return new Tone(new Note(noteLetter, Accidental.Sharp), octave); 
		} else if("b".equals(accString)){
			return new Tone(new Note(noteLetter, Accidental.Flat), octave);
		} else {
			return new Tone(new Note(noteLetter), octave);
		}
	}

	public static Note[] parseScale(String octaveSample) throws ParseException {
		String[] splitNotes = octaveSample.split("(?=[ABCDEFG])");
		Note[] notes = new Note[splitNotes.length-1];
		for(int i=0; i<notes.length; i++){
			notes[i] = parseNote(splitNotes[i+1]);
		}
		return notes;
	}
}
