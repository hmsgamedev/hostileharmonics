package com.basildsouza.tools.music.piano.engine.generator;

import com.basildsouza.tools.music.piano.engine.Clef;

/* TODO: This will be an interface that will contain methods to tell it what difficulty is required:
 * 
 * 
 */
public interface TuneGeneratorSelector {
	// When creating this, ideally it should also be passed in details for range, so that we know how far to generate
	public TuneGenerator getGenerator(Clef clef, int difficulty); //Perhaps also pass in range?
}
