package com.basildsouza.tools.music.piano.engine.generator;

import java.util.Arrays;
import java.util.Random;

//TODO: Make this a generic randomiser to use for treble also
public class IntervalRandomiser<T> {
	private final T[] choicesArray;
	private final double[] ranges;
	private int lastChosen = -1;
	
	private final Random randomGenerator = new Random(System.currentTimeMillis());
	
	public IntervalRandomiser(T[] choiceArray, double[] probabilityArray){
		if(choiceArray.length != probabilityArray.length || probabilityArray.length == 0){
			throw new IllegalStateException("Probabilities should be equal to number of choices, and greater than 0.");
		}
		
		this.choicesArray = choiceArray;
		ranges = new double[probabilityArray.length];
		double sum = 0;
		ranges[0] = probabilityArray[0];
		sum += ranges[0];
		for(int i=1; i<ranges.length; i++){
			ranges[i] = ranges[i-1] + probabilityArray[i];
			sum += probabilityArray[i];
		}
		
		if(Math.abs(sum -1) > 0.001){
			System.out.println(Arrays.toString(probabilityArray));
			throw new IllegalStateException("Sum of all probabilities should be equal to 1. Instead was: " + sum);
		}
	}
	
	public T[] getChoices() {
		return choicesArray;
	}
	
	public int getLastChosen() {
		return lastChosen;
	}
	
	public T getNextInterval(){
		double random = randomGenerator.nextDouble();
		for(int i=0; i<ranges.length; i++){
			if(random < ranges[i]){
				lastChosen = i;
				return choicesArray[i];
			}
		}
		lastChosen = randomGenerator.nextInt(ranges.length);
		return choicesArray[lastChosen];
	}
}
