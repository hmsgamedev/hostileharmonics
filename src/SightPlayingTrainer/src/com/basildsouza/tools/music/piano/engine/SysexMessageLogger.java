package com.basildsouza.tools.music.piano.engine;

import javax.sound.midi.SysexMessage;

public class SysexMessageLogger extends MidiMessageLogger{
	public String decodeMessage(SysexMessage message)
	{
		byte[]	abData = message.getData();
		String	strMessage = null;
		// System.out.println("sysex status: " + message.getStatus());
		if (message.getStatus() == SysexMessage.SYSTEM_EXCLUSIVE)
		{
			strMessage = "Sysex message: F0" + getHexString(abData);
		}
		else if (message.getStatus() == SysexMessage.SPECIAL_SYSTEM_EXCLUSIVE)
		{
			strMessage = "Continued Sysex message F7" + getHexString(abData);
			//seByteCount--; // do not count the F7
		}
		//seByteCount += abData.length + 1;
		//seCount++; // for the status byte
		return strMessage;
	}

}
