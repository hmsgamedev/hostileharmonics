package com.basildsouza.tools.music.piano.engine.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.basildsouza.tools.music.piano.engine.Tone;
import com.basildsouza.tools.music.piano.engine.io.Tune.Phrase;


public class TuneIO {
	protected static final String NAME = "name";
	protected static final String FAMILIARITY = "familiarity";
	protected static final String SCALE = "scale";
	protected static final String SCALE_STR = "scale.stringrep";
	protected static final String PHRASES = "phrases";
	
	protected static final String PHRASE_SEP = "|";
	protected static final String CLEF_SEP = "/";
	protected static final String TONE_SEP = ",";
	
	public void store(Tune tune, String tunePath) throws FileNotFoundException, IOException{
		Properties tuneProp = format(tune);
		
		File tuneFile = new File(tunePath);
		if(!tuneFile.getParentFile().exists()){
			throw new FileNotFoundException("Path to file does not exist!: " + tuneFile.getAbsolutePath());
		}
		
		FileOutputStream tuneStream = new FileOutputStream(tuneFile);
		try {
			tuneProp.store(tuneStream, "");
		} finally {
			tuneStream.close();
		}
	}
	
	public Properties format(Tune tune){
		Properties tuneProp = new Properties();
		
		tuneProp.setProperty(NAME, tune.getName());
		tuneProp.setProperty(FAMILIARITY, tune.getFamiliarity().toString());
		tuneProp.setProperty(SCALE_STR, tune.getScale().getOctaveSample());
		tuneProp.setProperty(SCALE, tune.getScale().toString());
		tuneProp.setProperty(PHRASES, phraseToProperty(tune.getPhraseList()));
		
		return tuneProp;
	}
	
	public Tune parse(Properties tuneProp) throws ParseException{
		return new Tune(tuneProp.getProperty(NAME), 
						Familiarity.valueOf(
								tuneProp.getProperty(FAMILIARITY, 
												     Familiarity.UNKNOWN.toString())), 
						Scale.valueOf(tuneProp.getProperty(SCALE)), 
						propertyToPhrase(tuneProp.getProperty(PHRASES)));
	}
	
	public Tune load(String tunePath) throws FileNotFoundException, IOException, ParseException{
		File tuneFile = new File(tunePath);
		if(!tuneFile.exists()){
			throw new FileNotFoundException("File does not exist!: " + tuneFile.getAbsolutePath());
		}
		
		Properties tuneProp = new Properties();
		FileInputStream inStream = new FileInputStream(tuneFile);
		try {
			tuneProp.load(inStream);
		} finally {
			inStream.close();
		}
		
		return parse(tuneProp);
	}

	private String phraseToProperty(List<Phrase> phraseList) {
		StringBuilder builder = new StringBuilder(500);
		for(Phrase phrase : phraseList){
			Tone[][] phraseArr = phrase.getPhrase();
			// Assumption is that both phrases are of same Lenght
			if(phraseArr.length != 2 || 
			   phraseArr[0].length != phraseArr[1].length){
				System.out.println("Phrase array of varying lengths found. " + Arrays.toString(phraseArr));
				System.out.println("Unable to write this. Skipping.");
				throw new IllegalStateException("Unable to write empty or mismatched phrases.");
			}
			for(int toneIdx=0; toneIdx<phraseArr[0].length; toneIdx++){
				Tone trebleTone = phraseArr[Phrase.TREBLE_PHRASE][toneIdx];
				Tone bassTone = phraseArr[Phrase.BASS_PHRASE][toneIdx];
				if(trebleTone != null){
					builder.append(trebleTone.toString());
				}
				builder.append(CLEF_SEP);
				if(bassTone != null){
				   builder.append(bassTone.toString());
				}
				builder.append(TONE_SEP);
			}
			builder.setLength(builder.length()-TONE_SEP.length());
			builder.append(PHRASE_SEP);
		}
		builder.setLength(builder.length()-PHRASE_SEP.length());
		
		return builder.toString();
	}

	private List<Phrase> propertyToPhrase(String phrasesStr) throws ParseException {
		String[] phraseStrArr = phrasesStr.split(PHRASE_SEP);
		List<Phrase> phraseList = new ArrayList<Phrase>(phraseStrArr.length);
		
		for(String phraseStr : phraseStrArr){
			String[] toneStrArr = phraseStr.split(TONE_SEP);
			Tone[][] toneArr = new Tone[2][toneStrArr.length];
			for(int toneIdx=0; toneIdx<toneStrArr.length; toneIdx++){
				String[] clefSepStrs = toneStrArr[toneIdx].split(CLEF_SEP);
				toneArr[Phrase.TREBLE_PHRASE][toneIdx] = MusicStringParser.parseTone(clefSepStrs[Phrase.TREBLE_PHRASE]);
				toneArr[Phrase.BASS_PHRASE][toneIdx] = MusicStringParser.parseTone(clefSepStrs[Phrase.BASS_PHRASE]);
			}
			
			phraseList.add(new Phrase(toneArr));
		}
		
		return phraseList;
	}
}
