package com.basildsouza.tools.music.piano.engine;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiUnavailableException;

import com.basildsouza.tools.music.piano.engine.event.MidiKeyEvent;
import com.basildsouza.tools.music.piano.engine.event.MidiKeyListener;

public class MidiEngine {
	private final MidiDevice midiDevice;
	private MidiEngineReceiver midiEngineReceiver;
	
	private Tone highestKeyPlayed;
	private Tone lowestKeyPlayed;

	public MidiEngine(MidiDevice midiDevice) throws MidiUnavailableException {
		this.midiDevice = midiDevice;

		if (midiDevice.getMaxTransmitters() == 0) {
			throw new IllegalArgumentException(
					"Cannot create a midi engine with device: "
							+ midiDevice.getDeviceInfo().getName()
							+ ". Need one capable of providing output.");
		}
		
		midiDevice.open();
		midiEngineReceiver = new MidiEngineReceiver();
		midiDevice.getTransmitter().setReceiver(midiEngineReceiver);
		registerCalibrationListener(midiEngineReceiver);
	}
	
	public void close(){
		midiEngineReceiver.close();
		midiDevice.close();
	}
	
	public void startCalibration(){
		highestKeyPlayed = null;
		lowestKeyPlayed = null;
	}
	
	public Tone[] getCalibrationResult(){
		return new Tone[] {highestKeyPlayed, lowestKeyPlayed};
	}
	
	private void registerCalibrationListener(MidiEngineReceiver midiEngineReceiver){
		midiEngineReceiver.addMidiKeyListenerList(new MidiKeyListener() {
			
			@Override
			public void midiKeyOn(MidiKeyEvent midiEvent) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void midiKeyOff(MidiKeyEvent midiEvent) {
				if(highestKeyPlayed == null || 
				   highestKeyPlayed.getTonicDistance() < midiEvent.getTone().getTonicDistance()){
					highestKeyPlayed = midiEvent.getTone();
				}
				if(lowestKeyPlayed == null || 
				   lowestKeyPlayed.getTonicDistance() > midiEvent.getTone().getTonicDistance()){
						lowestKeyPlayed = midiEvent.getTone();
				}
			}
		});
	}
	
	public Tone getHighestKeyPlayed() {
		return highestKeyPlayed;
	}
	
	public Tone getLowestKeyPlayed() {
		return lowestKeyPlayed;
	}

	public void addMidiKeyListener(MidiKeyListener listener){
		midiEngineReceiver.addMidiKeyListenerList(listener);
	}
}
