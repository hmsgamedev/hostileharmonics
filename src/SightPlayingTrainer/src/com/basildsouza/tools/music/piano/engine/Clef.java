package com.basildsouza.tools.music.piano.engine;


public enum Clef {
	Treble(new Tone(new Note(NoteLetter.E), 4)), // Octave is a guess, need to verify 
	Bass(new Tone(new Note(NoteLetter.G), 2)); // Octave is a guess, need to verify
	
	private final Tone bottomLineTone;
	private final int ledgerDistance;
	
	private Clef(Tone bottomLineTone) {
		this.bottomLineTone = bottomLineTone;
		this.ledgerDistance = getLedgerDistance(bottomLineTone);
	}
	
	public Tone getBottomLineNote() {
		return bottomLineTone;
	}
	
	private int getLedgerDistance(Tone tone){
		return tone.getOctave() * 7 + tone.getNote().getNote().ordinal();
	}
	
	/**
	 * Gives offset from bottom note. 
	 * Positive means upwards, negative means downwards. 
	 * Will need to be inverted for actual drawing
	 * @param tone
	 * @return
	 */
	public int getOffset(Tone tone){
		return getLedgerDistance(tone) - ledgerDistance; 
	}
}
