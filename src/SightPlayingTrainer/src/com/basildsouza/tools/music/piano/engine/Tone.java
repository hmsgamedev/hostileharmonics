package com.basildsouza.tools.music.piano.engine;

public class Tone {
    private final Note note; 
    private final int octave;
    
    //TODO: hard code tonic distance
    //TODO: make a getter method
    
    protected Tone(NoteLetter note, Accidental shift, int octave) {
    	this(new Note(note, shift), octave);
	}
    
    public Tone(Note note, int octave) {
    	this.note = note;
    	this.octave = octave;
    }
    
    public Note getNote() {
		return note;
	}

	public int getOctave() {
		return octave;
	}

	public boolean equals(Object obj) {
    	if(!(obj instanceof Tone)){
    		return false;
    	}
    	
    	Tone other = (Tone) obj;
    	
    	if(this.octave != other.octave){
    		return false;
    	}
    	
    	return this.note.equals(other.note);
    }
	
	public int getTonicDistance(){
		return this.octave * 12 + this.note.getTonicDistance();
	}
    
    public boolean equivalent(Tone other) {
    	// This compares if the modified notes are equal sounding
    	int thisOrdinal = this.octave*12 + this.note.getTonicDistance();
    	int otherOrdinal = other.octave*12 + other.note.getTonicDistance();
    	
    	return thisOrdinal == otherOrdinal;
    }
    
    @Override
    public String toString() {
    	//NoteLetter noteLetter = note.getNote();
    	//Accidental shift = note.getShift();
    	
    	//Was like this earlier return noteLetter.name() + octave + shift.toString();
		return note.toString() + octave;
    }
    
    public Tone toNatural(){
    	return new Tone(note.getNote(), Accidental.Natural, octave);
    }
    
    public Tone toAccidental(Accidental accidental){
    	return new Tone(note.getNote(), accidental, octave);
    }
    
    public Tone nextNatural(){
    	NoteLetter nextNoteLetter = note.getNote().nextNoteLetter();
    	if(note.getNote().ordinal() > nextNoteLetter.ordinal()){
    		// We wrapped over to the next octave
    		return new Tone(note.getNote().nextNoteLetter(), Accidental.Natural, octave+1);
    	} else {
    		return new Tone(note.getNote().nextNoteLetter(), Accidental.Natural, octave);
    	}
    }
    
    //TODO: Implement a previous natural, and other such methods  
    
    
    public static void main(String[] args){
    	Tone ASharp = new Tone(NoteLetter.A, Accidental.DoubleSharp, 4);
    	Tone BFlat = new Tone(NoteLetter.B, Accidental.Natural, 4);
    	System.out.println(ASharp + " " + BFlat);
    	
    	System.out.println("Equals: " + ASharp.equals(BFlat));
    	System.out.println("Equivalent: " + ASharp.equivalent(BFlat));
    }
    
}
