package com.basildsouza.tools.music.piano.engine.event;

public interface MidiKeyListener {
	public void midiKeyOn(MidiKeyEvent midiEvent);
	
	public void midiKeyOff(MidiKeyEvent midiEvent);
}
