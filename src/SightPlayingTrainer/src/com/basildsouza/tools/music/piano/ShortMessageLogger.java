package com.basildsouza.tools.music.piano;

import javax.sound.midi.ShortMessage;

import com.basildsouza.tools.music.piano.engine.MidiMessageLogger;

public class ShortMessageLogger extends MidiMessageLogger {
	private static final String[] KEY_NAMES = { "C", "C#", "D", "D#", "E", "F",
			"F#", "G", "G#", "A", "A#", "B" };

	private static final String[] SYSTEM_MESSAGE_TEXT = {
			"System Exclusive (should not be in ShortMessage!)",
			"MTC Quarter Frame: ", "Song Position: ", "Song Select: ",
			"Undefined", "Undefined", "Tune Request",
			"End of SysEx (should not be in ShortMessage!)", "Timing clock",
			"Undefined", "Start", "Continue", "Stop", "Undefined",
			"Active Sensing", "System Reset" };

	private static final String[] QUARTER_FRAME_MESSAGE_TEXT = {
			"frame count LS: ", "frame count MS: ", "seconds count LS: ",
			"seconds count MS: ", "minutes count LS: ", "minutes count MS: ",
			"hours count LS: ", "hours count MS: " };

	private static final String[] FRAME_TYPE_TEXT = { "24 frames/second",
			"25 frames/second", "30 frames/second (drop)",
			"30 frames/second (non-drop)", };

	public String decodeMessage(ShortMessage message) {
		String strMessage = null;
		switch (message.getCommand()) {
		case 0x80:
			strMessage = "note Off " + getKeyName(message.getData1())
					+ " velocity: " + message.getData2();
			break;

		case 0x90:
			strMessage = "note On " + getKeyName(message.getData1())
					+ " velocity: " + message.getData2();
			break;

		case 0xa0:
			strMessage = "polyphonic key pressure "
					+ getKeyName(message.getData1()) + " pressure: "
					+ message.getData2();
			break;

		case 0xb0:
			strMessage = "control change " + message.getData1() + " value: "
					+ message.getData2();
			break;

		case 0xc0:
			strMessage = "program change " + message.getData1();
			break;

		case 0xd0:
			strMessage = "key pressure " + getKeyName(message.getData1())
					+ " pressure: " + message.getData2();
			break;

		case 0xe0:
			strMessage = "pitch wheel change "
					+ get14bitValue(message.getData1(), message.getData2());
			break;

		case 0xF0:
			strMessage = SYSTEM_MESSAGE_TEXT[message.getChannel()];
			switch (message.getChannel()) {
			case 0x1:
				int nQType = (message.getData1() & 0x70) >> 4;
				int nQData = message.getData1() & 0x0F;
				if (nQType == 7) {
					nQData = nQData & 0x1;
				}
				strMessage += QUARTER_FRAME_MESSAGE_TEXT[nQType] + nQData;
				if (nQType == 7) {
					int nFrameType = (message.getData1() & 0x06) >> 1;
					strMessage += ", frame type: "
							+ FRAME_TYPE_TEXT[nFrameType];
				}
				break;

			case 0x2:
				strMessage += get14bitValue(message.getData1(),
						message.getData2());
				break;

			case 0x3:
				strMessage += message.getData1();
				break;
			}
			break;

		default:
			strMessage = "unknown message: status = " + message.getStatus()
					+ ", byte1 = " + message.getData1() + ", byte2 = "
					+ message.getData2();
			break;
		}
		if (message.getCommand() != 0xF0) {
			int nChannel = message.getChannel() + 1;
			String strChannel = "channel " + nChannel + ": ";
			strMessage = strChannel + strMessage;
		}
		// smCount++;
		// smByteCount+=message.getLength();
		return "[" + getHexString(message) + "] " + strMessage;
	}

	public static String getHexString(ShortMessage sm) {
		// bug in J2SDK 1.4.1
		// return getHexString(sm.getMessage());
		int status = sm.getStatus();
		String res = Integer.toHexString(sm.getStatus());
		// if one-byte message, return
		switch (status) {
		case 0xF6: // Tune Request
		case 0xF7: // EOX
			// System real-time messages
		case 0xF8: // Timing Clock
		case 0xF9: // Undefined
		case 0xFA: // Start
		case 0xFB: // Continue
		case 0xFC: // Stop
		case 0xFD: // Undefined
		case 0xFE: // Active Sensing
		case 0xFF:
			return res;
		}
		res += ' ' + Integer.toHexString(sm.getData1());
		// if 2-byte message, return
		switch (status) {
		case 0xF1: // MTC Quarter Frame
		case 0xF3: // Song Select
			return res;
		}
		switch (sm.getCommand()) {
		case 0xC0:
		case 0xD0:
			return res;
		}
		// 3-byte messages left
		res += ' ' + Integer.toHexString(sm.getData2());
		return res;
	}

	public static String getKeyName(int nKeyNumber) {
		if (nKeyNumber > 127) {
			return "illegal value";
		} else {
			int nNote = nKeyNumber % 12;
			int nOctave = nKeyNumber / 12;
			return KEY_NAMES[nNote] + (nOctave - 1);
		}
	}

	public static int get14bitValue(int nLowerPart, int nHigherPart) {
		return (nLowerPart & 0x7F) | ((nHigherPart & 0x7F) << 7);
	}

}
