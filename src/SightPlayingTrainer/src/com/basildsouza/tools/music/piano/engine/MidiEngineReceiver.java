package com.basildsouza.tools.music.piano.engine;

import java.util.LinkedList;
import java.util.List;

import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.SysexMessage;

import com.basildsouza.tools.music.piano.ShortMessageLogger;
import com.basildsouza.tools.music.piano.engine.event.MidiKeyEvent;
import com.basildsouza.tools.music.piano.engine.event.MidiKeyListener;

public class MidiEngineReceiver implements Receiver {
	private static final Note[] NOTE_ARR = {
		new Note(NoteLetter.C),
		new Note(NoteLetter.C, Accidental.Sharp),
		new Note(NoteLetter.D),
		new Note(NoteLetter.D, Accidental.Sharp),
		new Note(NoteLetter.E),
		new Note(NoteLetter.F),
		new Note(NoteLetter.F, Accidental.Sharp),
		new Note(NoteLetter.G),
		new Note(NoteLetter.G, Accidental.Sharp),
		new Note(NoteLetter.A),
		new Note(NoteLetter.A, Accidental.Sharp),
		new Note(NoteLetter.B),
	};
	
	private final ShortMessageLogger shortMessageLogger = new ShortMessageLogger();
	private final MetaMessageLogger metaMessageLogger = new MetaMessageLogger();
	private final SysexMessageLogger sysexMessageLogger = new SysexMessageLogger();

	//Change it later to be configured
	private boolean printTimeStampAsTicks = false;
	
	private void logMessage(MidiMessage message, long lTimeStamp){
		String strMessage = null;
		if (message instanceof ShortMessage) {
			strMessage = shortMessageLogger.decodeMessage((ShortMessage) message);
		} else if (message instanceof SysexMessage) {
			strMessage = sysexMessageLogger.decodeMessage((SysexMessage) message);
		} else if (message instanceof MetaMessage) {
			strMessage = metaMessageLogger.decodeMessage((MetaMessage) message);
		} else {
			strMessage = "unknown message type";
		}
		String strTimeStamp = null;
		
		if (printTimeStampAsTicks) {
			strTimeStamp = "tick " + lTimeStamp + ": ";
		} else {
			if (lTimeStamp == -1L) {
				strTimeStamp = "timestamp [unknown]: ";
			} else {
				strTimeStamp = "timestamp " + lTimeStamp + " us: ";
			}
		}
		
		String totalLogString = strTimeStamp + strMessage;
		//TODO: Log this
	}

	@Override
	public void send(MidiMessage message, long timestamp) {
		logMessage(message, timestamp);
		
		//TODO: Add further necesasry parts here
		if (message instanceof ShortMessage) {
			processMessage((ShortMessage) message, timestamp);
		}
	}

	@Override
	public void close() {
		midiKeyListenerList.clear();
	}
	
	private static final int NOTE_OFF_COMMAND = 0x80;
	private static final int NOTE_ON_COMMAND  = 0x90;
	private static final int POLYPHONIC_KEY_PRESSURE_COMMAND = 0xA0;
	private static final int CONTROL_CHANGE_COMMAND = 0xB0;
	private static final int PROGRAM_CHANGE_COMMAND = 0xC0;
	private static final int KEY_PRESSURE_COMMAND = 0xD0;
	private static final int PITCH_WHEEL_CHANGE_COMMAND = 0xD0;
	
	private final List<MidiKeyListener> midiKeyListenerList = new LinkedList<MidiKeyListener>();
	
	private void notifyMidiKeyListeners(Tone tone, boolean noteOn, long timestamp){
		for(MidiKeyListener listener : midiKeyListenerList){
			try {
				if(noteOn){
					listener.midiKeyOn(new MidiKeyEvent(tone, timestamp));
				} else {
					listener.midiKeyOff(new MidiKeyEvent(tone, timestamp));
				}
			} catch (Exception ex){
				//TODO: Log exception in the normal place since it would be caused by the program
				ex.printStackTrace();
			}
		}
	}
	
	public void processMessage(ShortMessage message, long timestamp) {
		String strMessage = null;
		switch (message.getCommand()) {
		case NOTE_OFF_COMMAND:
			//message.getData2() - For velocity
			notifyMidiKeyListeners(getKey(message.getData1()), false, timestamp);
			break;
		case NOTE_ON_COMMAND:
			//message.getData2() - For velocity
			notifyMidiKeyListeners(getKey(message.getData1()), true, timestamp);
			break;
/*
 * 		TODO: Make a generic way of firing these events
		case POLYPHONIC_KEY_PRESSURE_COMMAND:
			strMessage = "polyphonic key pressure "
					+ getKeyName(message.getData1()) + " pressure: "
					+ message.getData2();
			break;

		case CONTROL_CHANGE_COMMAND:
			strMessage = "control change " + message.getData1() + " value: "
					+ message.getData2();
			break;

		case 0xc0:
			strMessage = "program change " + message.getData1();
			break;

		case 0xd0:
			strMessage = "key pressure " + getKeyName(message.getData1())
					+ " pressure: " + message.getData2();
			break;

		case 0xe0:
			strMessage = "pitch wheel change "
					+ get14bitValue(message.getData1(), message.getData2());
			break;
*/
		case 0xF0:
			//8 == timing clock
			//14 == active sensing
			int channel = message.getChannel();
			//TODO: Handle this in a separate thread to maintain connection state

		}
		/*
		 * TODO: Might have to deal with channels later

		if (message.getCommand() != 0xF0) {
			int nChannel = message.getChannel() + 1;
			String strChannel = "channel " + nChannel + ": ";
			strMessage = strChannel + strMessage;
		}
		 */
		//TODO: Implement metrics
		//smCount++;
		//smByteCount += message.getLength();
	}
	
	public Tone getKey(int nKeyNumber) {
		if (nKeyNumber > 127) {
			return null;
		} else {
			int nNote = nKeyNumber % 12;
			int nOctave = nKeyNumber / 12;
			// TODO: Is this correct?
			return new Tone(NOTE_ARR[nNote], nOctave - 1);
		}
	}

	public void addMidiKeyListenerList(MidiKeyListener listener) {
		midiKeyListenerList.add(listener);
	}
}