package com.basildsouza.tools.music.piano.engine.chord;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.basildsouza.tools.music.piano.engine.Note;

public class Chord {
	private final List<Note> notes;
	private final Note root;
	private final String chordName;
	
	public static final Chord UNKNOWN_CHORD = new Chord(Arrays.asList((Note)null), null, "Unknown");
	
	
	public Chord(List<Note> notes, Note root, String chordName) {
		notes = new ArrayList<Note>(notes);
		Collections.sort(notes, new Comparator<Note>() {

			@Override
			public int compare(Note n1, Note n2) {
				int value = n1.getTonicDistance() - n2.getTonicDistance();
/*				Commented to support fifths
 * 				if(value == 0){
					throw new IllegalStateException("Chord cannot contain 2 same notes. (Atleast not in my engine :-) )");
				}
*/				return value;
			}
		});
		
		notes = Collections.unmodifiableList(notes);
		
		if(!notes.contains(root)){
			throw new IllegalStateException
						(chordName + " list provided: " + notes + 
						 " does not contain provided root: " + root);
		}
		this.notes = Collections.unmodifiableList(notes);
		this.root = root;
		this.chordName = chordName;
	}
	
	public String getChordName() {
		return chordName;
	}
	
	public List<Note> getNotes() {
		return notes;
	}
	
	public Note getRoot() {
		return root;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((notes == null) ? 0 : notes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Chord other = (Chord) obj;
		if (notes == null) {
			if (other.notes != null)
				return false;
		} else if (!notes.equals(other.notes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Chord ( " + chordName + ": " + notes + " - " + root + ")";
	}
}
