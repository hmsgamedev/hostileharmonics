package com.basildsouza.tools.music.piano.engine.io;

public enum Familiarity {
	FAMILIAR, RARE, UNKNOWN
}