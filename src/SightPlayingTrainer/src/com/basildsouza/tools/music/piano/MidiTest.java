package com.basildsouza.tools.music.piano;

import java.util.List;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiDevice.Info;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequencer;
import javax.sound.midi.Transmitter;

import com.basildsouza.tools.music.piano.engine.MidiDeviceLister;
import com.basildsouza.tools.music.piano.engine.MidiEngine;
import com.basildsouza.tools.music.piano.engine.PlayerMidiKeyListener;
import com.basildsouza.tools.music.piano.engine.chord.ChordBank;
import com.basildsouza.tools.music.piano.engine.chord.ChordDetector;
import com.basildsouza.tools.music.piano.engine.event.ChordEvent;
import com.basildsouza.tools.music.piano.engine.event.ChordListener;
import com.basildsouza.tools.music.piano.engine.event.ToneEvent;
import com.basildsouza.tools.music.piano.engine.event.ToneListener;

public class MidiTest {

	public static void main(String[] args) throws MidiUnavailableException, InterruptedException{
		Info[] infos = MidiSystem.getMidiDeviceInfo();
		Sequencer sequencer = MidiSystem.getSequencer();
		Transmitter transmitter;
		Receiver receiver;
		for(int i=0;i<infos.length;i++)
		{
//			if(infos[i].getName().contains("USB")){
				System.out.println(infos[i].getName() + " - " + infos[i].getDescription());
				Info usbInfo = infos[i];
				MidiDevice midiDevice = MidiSystem.getMidiDevice(usbInfo);
				System.out.println("Receivers: " + midiDevice.getMaxReceivers());
				System.out.println("Transmitters: " + midiDevice.getMaxTransmitters());
//			}
		    
		}
		
		int index = 0;
		
		List<MidiDevice> midiInputDevices = MidiDeviceLister.getInstance().getMidiInputDevices();
		MidiDevice midiDevice = midiInputDevices.get(0);
		if(midiInputDevices.size() >0){
			Info deviceInfo = midiDevice.getDeviceInfo();
			System.out.println("Connecting to: " + deviceInfo.getName());
			System.out.println("Receivers: " + midiDevice.getMaxReceivers());
			System.out.println("Transmitters: " + midiDevice.getMaxTransmitters());
			
			
			MidiEngine midiEngine = new MidiEngine(midiDevice);
			ChordBank chordBank = new ChordBank();
			ChordDetector chordDetector = new ChordDetector(chordBank);
			
			PlayerMidiKeyListener playerMidiKeyListener = new PlayerMidiKeyListener(chordDetector);
			
			midiEngine.addMidiKeyListener(playerMidiKeyListener);
			
			playerMidiKeyListener.addToneListener(new ToneListener() {
				
				@Override
				public void tonePlayed(ToneEvent te) {
					System.out.println("Tone played: " + te.getPlayedTone());
				}
			});
			
			playerMidiKeyListener.addChordListener(new ChordListener() {
				
				@Override
				public void chordPlayed(ChordEvent ce) {
					System.out.println("Chord Played: " + ce.getPlayedChord());
				}
			});
		} else {
			System.out.println("Too many device or no devices, please select the correct one!: " + midiInputDevices);
		}
		
		while(true){
			Thread.sleep(10000);
		}
	}
}
