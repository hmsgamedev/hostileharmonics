package com.basildsouza.tools.music.piano.engine;

public enum Accidental {
    DoubleFlat (-2) {
    	@Override
    	public String toString() {
    		return "bb";
    	}
    }
    , Flat(-1){
    	@Override
    	public String toString() {
    		return "b";
    	}
    }
    , Natural(0) {
    	@Override
    	public String toString() {
    		return "";
    	}

    }
    , 
    Sharp(1) {
    	@Override
    	public String toString() {
    		return "#";
    	}
    }
    , DoubleSharp(2){
    	@Override
    	public String toString() {
    		return "##";
    	}
    }
    ;
    // Double flats will not be often used
    
    private final int modifier;
    
    private Accidental(int modifier) {
		this.modifier = modifier;
	}
    
    public int getModifier() {
		return modifier;
	}
}
