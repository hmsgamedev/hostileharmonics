package com.basildsouza.tools.music.piano.engine.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.basildsouza.tools.music.piano.engine.Accidental;
import com.basildsouza.tools.music.piano.engine.Clef;
import com.basildsouza.tools.music.piano.engine.Note;
import com.basildsouza.tools.music.piano.engine.NoteLetter;
import com.basildsouza.tools.music.piano.engine.Tone;

public class RandomTuneGenerator implements TuneGenerator{
	private static final Clef[] RELAVANT_CLEFS = new Clef[]{Clef.Treble, Clef.Bass};
	private static final Accidental[] RELAVANT_ACCIDENTALS = new Accidental[]{Accidental.Sharp, Accidental.Natural, Accidental.Flat};
	
	private final String name;
	private final String description;
	private final Tone[][] selectedTones;
	private final IntervalRandomiser<Accidental> accidentalRandomiser;
	private final IntervalRandomiser<Clef> clefRandomiser;
	private final Random randomGenerator = new Random(System.currentTimeMillis());
	
	//TODO: Implement a biased generator towards the central tones
	public RandomTuneGenerator(String name, String description, 
							   Tone low, Tone high, 
							   IntervalRandomiser<Accidental> accidentalRandomiser, Clef clef) {
		this(name, description, new Tone[]{low}, new Tone[]{high}, 
			 accidentalRandomiser, new IntervalRandomiser<Clef>(new Clef[]{clef}, new double[]{1}));
	}
	
	public RandomTuneGenerator(String name, String description, 
			   Tone[] low, Tone[] high, 
			   IntervalRandomiser<Accidental> accidentalRandomiser, IntervalRandomiser<Clef> clefRandomiser) {
		
		final int numClefs = clefRandomiser.getChoices().length;
		if(low.length != high.length || low.length != numClefs){
			throw new IllegalStateException("Tone arrays high and low should be equal to each other and number to number of clefs: " + numClefs);
		}
		
		this.name = name;
		this.description = description;
		this.selectedTones = new Tone[numClefs][];
		for(int i=0; i<selectedTones.length; i++){
			this.selectedTones[i] = generateToneArray(low[i].toNatural(), high[i].toNatural());
		}
		
		this.accidentalRandomiser = accidentalRandomiser;
		this.clefRandomiser = clefRandomiser;
	}
	private static Tone[] generateToneArray(Tone low, Tone high){
		List<Tone> tempToneList = new ArrayList<Tone>(high.getTonicDistance() - low.getTonicDistance() + 1);
		tempToneList.add(low.toNatural());
		Tone currTone = low.nextNatural();
		while(currTone.getTonicDistance() <= high.getTonicDistance()){
			tempToneList.add(currTone);
			currTone = currTone.nextNatural();
		}
		return tempToneList.toArray(new Tone[0]);
	}

	//TODO: Store last generated tone?
	@Override
	public GeneratedTone getNextTone() {
		Clef clef = clefRandomiser.getNextInterval();
		int chosenClef = clefRandomiser.getLastChosen();
		
		Tone tone = selectedTones[chosenClef][randomGenerator.nextInt(selectedTones[chosenClef].length)];
		tone = tone.toAccidental(accidentalRandomiser.getNextInterval());
		return new GeneratedToneImpl(clef, tone);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}
	
	private static class GeneratedToneImpl implements GeneratedTone {
		private final Tone tone;
		private final Clef clef;
		
		public GeneratedToneImpl(Clef clef, Tone tone) {
			this.clef = clef;
			this.tone = tone;
		}
		
		@Override
		public Tone getTone() {
			return tone;
		}
		
		@Override
		public Clef getClef() {
			return clef;
		}
		
		@Override
		public String toString() {
			
			return clef.toString() + " " + tone.toString();
		}
	}
	
	public static final RandomTuneGenerator TREBLE_CENTRAL_NOTES 
					= new RandomTuneGenerator("Simple Notes", "Only Notes from the main treble line from middle C. No Accidentals", 
					 						  new Tone(new Note(NoteLetter.C), 4), 
					 						  new Tone(new Note(NoteLetter.F), 5), 
					 						  new IntervalRandomiser<Accidental>(RELAVANT_ACCIDENTALS, new double[]{0, 1, 0}), Clef.Treble);
	   
	public static final RandomTuneGenerator TREBLE_CENTRAL_NOTES_WITH_ACCIDENTALS 
				   = new RandomTuneGenerator("Simple Notes", "Only Notes from the main treble line from middle C. No Accidentals", 
	 						  new Tone(new Note(NoteLetter.C), 4), 
	 						  new Tone(new Note(NoteLetter.F), 5), 
	 						  new IntervalRandomiser<Accidental>(RELAVANT_ACCIDENTALS, new double[]{0.25, 0.5, 0.25}), Clef.Treble);
	
	public static final RandomTuneGenerator BASS_CENTRAL_NOTES 
					= new RandomTuneGenerator("Simple Notes", "Only Notes from the main treble line from middle C. No Accidentals", 
	 						  new Tone(new Note(NoteLetter.G), 2),  
	 						  new Tone(new Note(NoteLetter.C), 4), 
	 						  new IntervalRandomiser<Accidental>(RELAVANT_ACCIDENTALS, new double[]{0, 1, 0}), Clef.Bass);

	public static final RandomTuneGenerator BASS_CENTRAL_NOTES_WITH_ACCIDENTALS 
		   			= new RandomTuneGenerator("Simple Notes", "Only Notes from the main treble line from middle C. No Accidentals", 
		   					  new Tone(new Note(NoteLetter.G), 2), 
		   					  new Tone(new Note(NoteLetter.C), 4), 
		   					  new IntervalRandomiser<Accidental>(RELAVANT_ACCIDENTALS, new double[]{0.25, 0.5, 0.25}), Clef.Bass);

	public static final RandomTuneGenerator GRAND_CENTRAL_NOTES 
					= new RandomTuneGenerator("Simple Notes", "Only Notes from the main treble line from middle C. No Accidentals", 
							  new Tone[]{new Tone(new Note(NoteLetter.C), 4), new Tone(new Note(NoteLetter.G), 2)}, 
							  new Tone[]{new Tone(new Note(NoteLetter.F), 5), new Tone(new Note(NoteLetter.A), 3)}, 
							  new IntervalRandomiser<Accidental>(RELAVANT_ACCIDENTALS, new double[]{0, 1, 0}), 
							  new IntervalRandomiser<Clef>(RELAVANT_CLEFS, new double[]{0.5, 0.5}));

	public static final RandomTuneGenerator GRAND_CENTRAL_NOTES_WITH_ACCIDENTALS 
						= new RandomTuneGenerator("Simple Notes", "Only Notes from the main treble line from middle C. No Accidentals", 
							  new Tone[]{new Tone(new Note(NoteLetter.C), 4), new Tone(new Note(NoteLetter.G), 2)}, 
							  new Tone[]{new Tone(new Note(NoteLetter.F), 5), new Tone(new Note(NoteLetter.A), 3)}, 
							  new IntervalRandomiser<Accidental>(RELAVANT_ACCIDENTALS, new double[]{0.25, 0.5, 0.25}), 
							  new IntervalRandomiser<Clef>(RELAVANT_CLEFS, new double[]{0.5, 0.5}));
	/**
	 * Test cases:
	 * 1. Pass in same note
	 * 2. Pass in different nioes, ensure the range is proper (by passing in a large neough set???
	 * 3. 
	 * 
	 * @param args
	 */
	public static void main(String[] args){
		IntervalRandomiser<Accidental> accidentalRandomiser2 = new IntervalRandomiser<Accidental>(RELAVANT_ACCIDENTALS, new double[]{0.25, 0.5, 0.25});
		//RandomTuneGenerator rtg = new RandomTuneGenerator("Test", "Test Generator", new Tone(new Note(NoteLetter.C), 4), new Tone(new Note(NoteLetter.C), 5), accidentalRandomiser2);
		RandomTuneGenerator rtg = RandomTuneGenerator.TREBLE_CENTRAL_NOTES;
		
		for(int i=0; i<100; i++){
			System.out.println(rtg.getNextTone());
		}
	}

	@Override
	public GeneratedTone getNextTone(double multiplier) {
		return getNextTone(); // Full random anyways, so doesnt make a difference
	}
	

}
