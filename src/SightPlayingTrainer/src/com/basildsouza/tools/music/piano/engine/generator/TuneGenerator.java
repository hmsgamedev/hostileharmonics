package com.basildsouza.tools.music.piano.engine.generator;

import com.basildsouza.tools.music.piano.engine.Clef;
import com.basildsouza.tools.music.piano.engine.Tone;

public interface TuneGenerator {
	public GeneratedTone getNextTone();
	public GeneratedTone getNextTone(double multiplier);
	public String getName();
	public String getDescription();
	
	public static interface GeneratedTone{
		Tone getTone();
		Clef getClef();
	}
}
