package com.basildsouza.tools.music.piano.engine.event;

import com.basildsouza.tools.music.piano.engine.chord.PlayedChord;


public class ChordEvent {
	private final PlayedChord playedChord;
	private final long time;

	public ChordEvent(final PlayedChord playedChord, final long time) {
		this.playedChord = playedChord;
		this.time = time;
	}
	
	public PlayedChord getPlayedChord() {
		return playedChord;
	}
	
	public long getTime() {
		return time;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((playedChord == null) ? 0 : playedChord.hashCode());
		result = prime * result + (int) (time ^ (time >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChordEvent other = (ChordEvent) obj;
		if (playedChord == null) {
			if (other.playedChord != null)
				return false;
		} else if (!playedChord.equals(other.playedChord))
			return false;
		if (time != other.time)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ChordEvent [playedChord=" + playedChord + ", time=" + time
				+ "]";
	}
	
	
	
	
}
