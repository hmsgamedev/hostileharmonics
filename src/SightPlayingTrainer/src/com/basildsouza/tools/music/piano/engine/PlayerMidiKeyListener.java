package com.basildsouza.tools.music.piano.engine;

import java.util.LinkedList;
import java.util.List;

import com.basildsouza.tools.music.piano.engine.chord.ChordDetector;
import com.basildsouza.tools.music.piano.engine.chord.PlayedChord;
import com.basildsouza.tools.music.piano.engine.event.ChordEvent;
import com.basildsouza.tools.music.piano.engine.event.ChordListener;
import com.basildsouza.tools.music.piano.engine.event.MidiKeyEvent;
import com.basildsouza.tools.music.piano.engine.event.MidiKeyListener;
import com.basildsouza.tools.music.piano.engine.event.PlayerDevice;
import com.basildsouza.tools.music.piano.engine.event.ToneEvent;
import com.basildsouza.tools.music.piano.engine.event.ToneListener;

public class PlayerMidiKeyListener implements MidiKeyListener, PlayerDevice {
	private static final int MIN_CHORD_SIZE = 3;
	private static final int MAX_CHORD_SIZE = 7;
	private final Tone high;
	private final Tone low;
	
	private final List<Tone> pressedTonesList = new LinkedList<Tone>();
	private List<Tone> chordTonesList = new LinkedList<Tone>();
	
	private final List<ToneListener> toneListeners = new LinkedList<ToneListener>();
	private final List<ChordListener> chordListeners = new LinkedList<ChordListener>();
	
	private final ChordDetector chordDetector;
	
	public PlayerMidiKeyListener(ChordDetector chordDetector){
		//TODO: replace this with the lowest and higest tone possible on a piano
		this(chordDetector, null, null);
	}
	
	public PlayerMidiKeyListener(ChordDetector chordDetector, Tone high, Tone low){
		this.high = high;
		this.low = low;
		this.chordDetector = chordDetector;
	}
	
	public void addToneListener(ToneListener listener){
		toneListeners.add(listener);
	}
	
	public void addChordListener(ChordListener listener){
		chordListeners.add(listener);
	}
	
	@Override
	public void removeToneListener(ToneListener listener) {
		toneListeners.remove(listener);
	}

	@Override
	public void removeChordListener(ChordListener listener) {
		chordListeners.remove(listener);
	}

	
	private void notifyToneListners(Tone tonePlayed, long timestamp){
		final ToneEvent te = new ToneEvent(tonePlayed, timestamp); 
		for(ToneListener listener : toneListeners){
			try{
				listener.tonePlayed(te);
			} catch(Exception ex){
				//TODO: Log in normal place since it is program (log with warn and say that processing will continue
				ex.printStackTrace();
			}
		}
	}
	
	private void notifyChordListners(PlayedChord chordPlayed, long timestamp){
		final ChordEvent ce = new ChordEvent(chordPlayed, timestamp); 
		for(ChordListener listener : chordListeners){
			try{
				listener.chordPlayed(ce);
			} catch(Exception ex){
				//TODO: Log in normal place since it is program (log with warn and say that processing will continue
				ex.printStackTrace();
			}
		}
	}
	
	
	public Tone getHigh() {
		return high;
	}
	
	public Tone getLow() {
		return low;
	}
	
	private boolean isRelavant(Tone playedTone){
		if(high != null && high.getTonicDistance() < playedTone.getTonicDistance() ){
			return false;
		}
		
		if(low != null && low.getTonicDistance() > playedTone.getTonicDistance() ){
			return false;
		}
		System.out.println("return true");
		return true;
	}
	
	@Override
	public void midiKeyOn(MidiKeyEvent te) {
		System.out.println("Midi Key On: " + te.getTone());
		if(!isRelavant(te.getTone())){
			return;
		}
		Tone toneOn = te.getTone();
		if(pressedTonesList.contains(toneOn)){
			//Fake or missed event?
			return;
		}
		if(chordTonesList.size() >= MAX_CHORD_SIZE){
			// To many keys have been pressed, just ignore the built up chord
			System.out.println("Too many tones!!!. Waiting for: " + pressedTonesList + " In Chord: " + chordTonesList);
			pressedTonesList.removeAll(chordTonesList);
			chordTonesList = new LinkedList<Tone>();
			//TODO: Log an event here
		}
		pressedTonesList.add(toneOn);
		chordTonesList.add(toneOn);
	}	

	@Override
	public void midiKeyOff(MidiKeyEvent te) {
		System.out.println("Midi Key Off: " + te.getTone());
		if(!isRelavant(te.getTone())){
			return;
		}
		final Tone toneOff = te.getTone();
		
		if(pressedTonesList.isEmpty()){
			//Wierd situation, where nothign was pressed but a key was released. Still firing a simple event.
			// This might only happen in case of a bad chord firing (too many keys pressed)
			notifyToneListners(toneOff, te.getTime());
			return;
		}
		
		pressedTonesList.remove(toneOff);
		
		if(!pressedTonesList.isEmpty() && chordTonesList.size() >= MIN_CHORD_SIZE){
			// Means a chord is in progress
			return;
		} 
		
		if(chordTonesList.size() >= MIN_CHORD_SIZE){
			PlayedChord playedChord = chordDetector.detectChord(chordTonesList);
			notifyChordListners(playedChord, te.getTime());
			chordTonesList = new LinkedList<Tone>();
		} else {
			notifyToneListners(toneOff, te.getTime());
			chordTonesList.remove(toneOff);
		}
	}
}
