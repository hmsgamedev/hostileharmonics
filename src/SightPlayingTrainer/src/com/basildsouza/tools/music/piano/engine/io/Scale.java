package com.basildsouza.tools.music.piano.engine.io;

import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.basildsouza.tools.music.piano.engine.Accidental;
import com.basildsouza.tools.music.piano.engine.Note;
import com.basildsouza.tools.music.piano.engine.NoteLetter;

/**
 * http://www.basicmusictheory.com/q/16/p/mj
 * @author Basil Dsouza
 *
 */
public enum Scale {
	B_Sharp("C##D##E#F##G##A##B#", new Note(NoteLetter.B, Accidental.Sharp)), 
	C("CDEFGAB", new Note(NoteLetter.C)),
	
	C_Sharp("C#D#E#F#G#A#B#", new Note(NoteLetter.C, Accidental.Sharp)), 
	D_Flat("CDbEbFGbAbBb", new Note(NoteLetter.D, Accidental.Flat)),
	
	D("C#DEF#GAB", new Note(NoteLetter.D, Accidental.Natural)),
	
	D_Sharp("C##D#E#F##G#A#B#", new Note(NoteLetter.D, Accidental.Sharp)), 
	E_Flat("CDEbFGAbBb", new Note(NoteLetter.E, Accidental.Flat)),
	
	E("C#DEF#G#AB", new Note(NoteLetter.E, Accidental.Natural)), 
	F_Flat("CbDbEFbGbAbBbb", new Note(NoteLetter.F, Accidental.Flat)),
	
	E_Sharp("C##D##E#F##G##A#B#", new Note(NoteLetter.E, Accidental.Sharp)), 
	F("CDEFGABb", new Note(NoteLetter.F, Accidental.Natural)),
	
	F_Sharp("C#D#E#F#G#A#B", new Note(NoteLetter.F, Accidental.Sharp)), 
	G_Flat("CbDbEbFGbAbBb", new Note(NoteLetter.G, Accidental.Flat)),
	
	G("CDEF#GAB", new Note(NoteLetter.G, Accidental.Natural)),
	
	G_Sharp("C#D#E#F##G#A#B#", new Note(NoteLetter.G, Accidental.Sharp)), 
	A_Flat("CDbEbFGABbb", new Note(NoteLetter.A, Accidental.Flat)),
	
	A("C#DEF#G#AB", new Note(NoteLetter.A, Accidental.Natural)),
	
	A_Sharp("C##D#E#F##G##A#B#", new Note(NoteLetter.A, Accidental.Sharp)),
	B_Flat("CDEbFGABb", new Note(NoteLetter.B, Accidental.Flat)),
	
	B("C#D#EF#G#A#B", new Note(NoteLetter.B, Accidental.Natural)), 
	C_Flat("CbDbEbFbGbAbBb", new Note(NoteLetter.C, Accidental.Flat));
	
	private final int distanceFromC; 
	private final String octaveSample;
	private final Note startNote;
	private final List<Note> noteList; // This might not be required
	
	private final Note BASE_NOTE = new Note(NoteLetter.C);
	
	private Scale(String octaveSample, Note startNote) {
		this.octaveSample = octaveSample; 
		this.startNote = startNote;
		try {
			this.noteList = Arrays.asList(MusicStringParser.parseScale(octaveSample));
		} catch (ParseException e) {
			// Unexpected
			throw new IllegalStateException("Scale not valid for parsing: " + octaveSample);
		}
		
		this.distanceFromC = startNote.getTonicDistance() - BASE_NOTE.getTonicDistance(); // This might even be negative actually? 
	}
	
	public int getDistanceFromC() {
		return distanceFromC;
	}
	
	public List<Note> getNoteList() {
		return noteList;
	}
	
	public String getOctaveSample() {
		return octaveSample;
	}
	
	public Note getStartNote() {
		return startNote;
	}
	
	public Note getScaledNote(Note note){
		for(Note currNote : noteList){
			if(currNote.equivalent(note)){
				return currNote;
			}
		}
		return note;
	}
	
	public static Scale getScaleByOctaveSample(String octaveSample){
		return SCALE_MAP.get(octaveSample);
	}
	
	private static final Map<String, Scale> SCALE_MAP;

	static {
		SCALE_MAP = new HashMap<String, Scale>();
		for(Scale scale : Scale.values()){
			SCALE_MAP.put(scale.getOctaveSample(), scale);
		}
	}

}