package com.basildsouza.tools.music.piano.engine.event;

import com.basildsouza.tools.music.piano.engine.Tone;

public interface PlayerDevice {
	public void addToneListener(ToneListener listener);
	public void addChordListener(ChordListener listener);
	
	public void removeToneListener(ToneListener listener);
	public void removeChordListener(ChordListener listener);

	
	public Tone getHigh();
	public Tone getLow();
}
