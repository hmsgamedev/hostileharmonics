package com.basildsouza.tools.music.piano.engine;

public interface ToneChecker {
	public boolean compareTones(Tone playedTone, Tone expectedTone);
}
