package com.basildsouza.tools.music.piano.engine.chord;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.basildsouza.tools.music.piano.engine.Note;
import com.basildsouza.tools.music.piano.engine.Tone;

public class ChordDetector {
	private final Map<String, Chord> chordMap;
	
	public ChordDetector(ChordBank chordBank) {
		chordMap = buildChordMap(chordBank.getAllRegisteredChords());
	}
	
	private Map<String, Chord> buildChordMap(List<Chord> chordList){
		Map<String, Chord> chordMap = new HashMap<String, Chord>();
		
		for(Chord chord : chordList){
			Chord olderOne = chordMap.put(buildChordKey(chord.getNotes()), chord);
			if(olderOne != null){
				throw new IllegalStateException
								("Two chords found with similar signature. " + 
								 olderOne + " and " + chord);
			}
		}
		
		return chordMap; 
	}
	
	private String buildChordKey(List<Note> notes){
		StringBuilder sb = new StringBuilder(20);
		 
		for(Note note : notes){
			sb.append(note.getTonicDistance()).append("-");
		}
		sb.setLength(sb.length()-1);
		return sb.toString();
	}
	
	
	public PlayedChord detectChord(List<Tone> playedTones){
		List<Note> playedNotes = new ArrayList<Note>(playedTones.size());
		for(Tone tone : playedTones){
			playedNotes.add(tone.getNote());
		}
		
		Collections.sort(playedNotes, new Comparator<Note>() {
			@Override
			public int compare(Note n1, Note n2) {
				return n1.getTonicDistance() - n2.getTonicDistance();
			}
		});
		
		// Detect Chord;
		String chordMapKey = buildChordKey(playedNotes);
		Chord chord = chordMap.get(chordMapKey);
		if(chord == null){
			return new PlayedChord(Chord.UNKNOWN_CHORD, playedTones, false); 
		}
		return new PlayedChord(chord, playedTones, false);
	}
	
/*	public static void main(String[] args){
		ChordDetector detector = new ChordDetector(new ChordBank());
		
		final Note CN = new Note(C, Natural);
		final Note CS = new Note(C, Sharp);
		final Note DN = new Note(D, Natural);
		final Note DS = new Note(D, Sharp);
		final Note EN = new Note(E, Natural);
		final Note FN = new Note(F, Natural);
		final Note FS = new Note(F, Sharp);
		final Note GN = new Note(G, Natural);
		final Note GS = new Note(G, Sharp);
		final Note AN = new Note(A, Natural);
		final Note AS = new Note(A, Sharp);
		final Note BN = new Note(B, Natural);

		
		System.out.println(detector.detectChord(Arrays.asList(new Tone(CN, 5), new Tone(EN, 5), new Tone(GN, 7))));
	}
*/
}
