package com.basildsouza.tools.music.piano.engine;

public enum NoteLetter {
    C(0), D(2), E(4), F(5), G(7), A(9), B(11);
    
    private final int tonicDistance;
    
    private NoteLetter(int tonicDistance) {
		this.tonicDistance = tonicDistance;
	}
    
    public int getTonicDistance() {
		return tonicDistance;
	}
    
    public NoteLetter nextNoteLetter(){
    	NoteLetter[] values = NoteLetter.values();
    	return values[(ordinal()+1)% values.length];
    }
}
