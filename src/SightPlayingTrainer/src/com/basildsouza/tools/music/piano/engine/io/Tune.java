package com.basildsouza.tools.music.piano.engine.io;

import java.util.LinkedList;
import java.util.List;

import com.basildsouza.tools.music.piano.engine.Tone;

public class Tune {
	private final String name;
	private final Familiarity familiarity;
	private final Scale scale;
	
	private final List<Phrase> phraseList;
	
	public Tune(String name, Familiarity familiarity, Scale scale, List<Phrase> phraseList){
		this.name = name;
		this.familiarity = familiarity;
		this.scale = scale;
		this.phraseList = phraseList;
	}
	
	public Tune(String name, Familiarity familiarity, Scale scale){
		this(name, familiarity, scale, new LinkedList<Tune.Phrase>());
	}
	
	public Familiarity getFamiliarity() {
		return familiarity;
	}
	
	public String getName() {
		return name;
	}
	
	public List<Phrase> getPhraseList() {
		return phraseList;
	}
	
	public void addOnlyTreblePhrase(Tone[] treblePhrase){
		Tone[][] phrase = new Tone[2][treblePhrase.length];
		System.arraycopy(treblePhrase, 0, phrase[Phrase.TREBLE_PHRASE], 0, treblePhrase.length);
		addPhrase(phrase);
	}
	
	public void addPhrase(Tone[][] phrase){
		for(int clef=0; clef<phrase.length; clef++){
			for(int toneIdx=0;toneIdx<phrase[clef].length; toneIdx++){
				final Tone originalTone = phrase[clef][toneIdx];
				phrase[clef][toneIdx] 
							= new Tone(scale.getScaledNote(originalTone.getNote()), 
							 		   originalTone.getOctave());
			}
		}
		
		phraseList.add(new Phrase(phrase));
	}
	
	public Scale getScale() {
		return scale;
	}
	
	public static class Phrase {
		public static final int TREBLE_PHRASE = 0;
		public static final int BASS_PHRASE = 1;
		
		private final Tone[][] phrase;
		
		public Phrase(Tone[][] phrase) {
			if(phrase.length != 2){
				throw new IllegalStateException("Expected 2xN array. Instead got: " + phrase.length);
			}
			this.phrase = phrase;
		}
		
		public Tone[][] getPhrase() {
			return phrase;
		}
		
		public Tone[] getTreblePhrase() {
			return phrase[TREBLE_PHRASE];
		}
		
		public Tone[] getBassPhrase() {
			return phrase[BASS_PHRASE];
		}
	}
}
