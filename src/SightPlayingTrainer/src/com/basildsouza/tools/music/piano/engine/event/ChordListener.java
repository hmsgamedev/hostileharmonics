package com.basildsouza.tools.music.piano.engine.event;

public interface ChordListener {
	public void chordPlayed(ChordEvent ce);
}
