package com.basildsouza.tools.music.piano.engine;

public class Note {
    private final NoteLetter note; 
    private final Accidental shift;
    
    //TODO: Add a sharper / Flatter
    // Add a next note
    
    public Note(NoteLetter note) {
		this(note, Accidental.Natural);
	}
    
    public Note(NoteLetter note, Accidental shift) {
		this.note = note;
		this.shift = shift;
	}
    
    public NoteLetter getNote() {
		return note;
	}
    
    public Accidental getShift() {
		return shift;
	}
    
    public int getTonicDistance(){
    	return this.note.getTonicDistance() + this.shift.getModifier();
    }
    
    public boolean equivalent(Note other) {
    	int thisTonic  = (this.note.getTonicDistance() + this.shift.getModifier() + 12) % 12;
    	int otherTonic = (other.note.getTonicDistance() + other.shift.getModifier() + 12) % 12;
    	
    	return thisTonic == otherTonic;
    }
    
    
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((note == null) ? 0 : note.hashCode());
		result = prime * result + ((shift == null) ? 0 : shift.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Note other = (Note) obj;
		if (note != other.note)
			return false;
		if (shift != other.shift)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return note.name() + shift.toString();
	}
}
