package com.basildsouza.tools.music.piano.engine;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;

public class MidiDeviceLister {
	private List<MidiDevice> inputDevices;
	private List<MidiDevice> outputDevices;
	private List<String> failedDeviceNames;

	private static final MidiDeviceLister instance = new MidiDeviceLister();

	//TODO: Move to spring?
	public static MidiDeviceLister getInstance() {
		return instance;
	}

	private MidiDeviceLister() {
		refresh();
	}

	/**
	 * Lists out all of the devices connected to this 
	 * computer from which this program can receive input.
	 * 
	 * refresh needs to be explicitly called before this checks for new devices.
	 * @return List of devices from which the program can receive input
	 */
	public synchronized List<MidiDevice> getMidiInputDevices() {
		return inputDevices;
	}

	/**
	 * Lists out all of the devices connected to this 
	 * computer to which this program can send output
	 * 
	 * Refresh needs to be explicitly called before this checks for new devices.
	 * 
	 * @return List of devices from which the program can send output
	 */
	public synchronized List<MidiDevice> getMidiOutputDevices() {
		return outputDevices;
	}

	public synchronized List<String> getFailedDeviceNames() {
		return failedDeviceNames;
	}

	public synchronized void refresh() {
		MidiDevice.Info[] deviceInfoArr = MidiSystem.getMidiDeviceInfo();
		final List<MidiDevice> tempInputDevice = new LinkedList<MidiDevice>();
		final List<MidiDevice> tempOutputDevice = new LinkedList<MidiDevice>();
		final List<String> tempFailedList = new LinkedList<String>();

		for (MidiDevice.Info deviceInfo : deviceInfoArr) {
			try {
				MidiDevice midiDevice = MidiSystem.getMidiDevice(deviceInfo);
				if (midiDevice.getMaxTransmitters() != 0) {
					tempInputDevice.add(midiDevice);
				} else if (midiDevice.getMaxReceivers() != 0) {
					tempOutputDevice.add(midiDevice);
				}
			} catch (MidiUnavailableException e) {
				// TODO: WARN: Unable to get MIDI device for: " +
				// deviceInfo.getName(). Not considering for this list
				e.printStackTrace();
				tempFailedList.add(deviceInfo.getName());
			}
		}

		inputDevices = Collections.unmodifiableList(tempInputDevice);
		outputDevices = Collections.unmodifiableList(tempOutputDevice);
		failedDeviceNames = Collections.unmodifiableList(tempFailedList);
	}
}
