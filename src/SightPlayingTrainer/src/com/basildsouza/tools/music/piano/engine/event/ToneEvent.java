package com.basildsouza.tools.music.piano.engine.event;

import com.basildsouza.tools.music.piano.engine.Tone;


public class ToneEvent {
	private final Tone playedTone;
	private final long time;

	public ToneEvent(final Tone playedTone, final long time) {
		this.playedTone = playedTone;
		this.time = time;
	}
	
	public Tone getPlayedTone() {
		return playedTone;
	}
	
	public long getTime() {
		return time;
	}
	
	

	@Override
	public String toString() {
		return "ToneEvent [playedTone=" + playedTone + ", time=" + time + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((playedTone == null) ? 0 : playedTone.hashCode());
		result = prime * result + (int) (time ^ (time >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ToneEvent other = (ToneEvent) obj;
		if (playedTone == null) {
			if (other.playedTone != null)
				return false;
		} else if (!playedTone.equals(other.playedTone))
			return false;
		if (time != other.time)
			return false;
		return true;
	}
	
	
}
