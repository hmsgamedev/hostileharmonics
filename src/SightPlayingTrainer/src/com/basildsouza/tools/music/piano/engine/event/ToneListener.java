package com.basildsouza.tools.music.piano.engine.event;


public interface ToneListener {
	public void tonePlayed(ToneEvent te);
}
