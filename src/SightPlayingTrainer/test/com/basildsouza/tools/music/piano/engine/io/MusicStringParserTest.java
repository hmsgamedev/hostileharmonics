package com.basildsouza.tools.music.piano.engine.io;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;
import org.junit.internal.ArrayComparisonFailure;

import com.basildsouza.tools.music.piano.engine.Accidental;
import com.basildsouza.tools.music.piano.engine.Note;
import com.basildsouza.tools.music.piano.engine.NoteLetter;
import com.basildsouza.tools.music.piano.engine.Tone;

public class MusicStringParserTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testParseNaturalTone() throws ParseException {
		assertEquals("Tone not parsed properly", 
					 new Tone(new Note(NoteLetter.C), 5), 
					 MusicStringParser.parseTone("C5"));
	}
	
	@Test
	public void testParseSharpTone() throws ParseException {
		assertEquals("Tone not parsed properly", 
					 new Tone(new Note(NoteLetter.C, Accidental.Sharp), 5), 
					 MusicStringParser.parseTone("C#5"));
	}

	@Test
	public void testParseDoubleSharpTone() throws ParseException {
		assertEquals("Tone not parsed properly", 
					 new Tone(new Note(NoteLetter.C, Accidental.DoubleSharp), 5), 
					 MusicStringParser.parseTone("C##5"));
	}
	
	@Test
	public void testParseFlatTone() throws ParseException {
		assertEquals("Tone not parsed properly", 
					 new Tone(new Note(NoteLetter.C, Accidental.Flat), 5), 
					 MusicStringParser.parseTone("Cb5"));
	}
	
	@Test
	public void testParseDoubleFlatTone() throws ParseException {
		assertEquals("Tone not parsed properly", 
					 new Tone(new Note(NoteLetter.C, Accidental.DoubleFlat), 5), 
					 MusicStringParser.parseTone("Cbb5"));
	}
	
	@Test
	public void testParseNaturalNote() throws Exception{
		assertEquals("Note not parsed properly", 
					 new Note(NoteLetter.C), 
					 MusicStringParser.parseNote("C"));
	}
	
	@Test
	public void testParseSharpNote() throws ParseException {
		assertEquals("Note not parsed properly", 
					 new Note(NoteLetter.C, Accidental.Sharp), 
					 MusicStringParser.parseNote("C#"));
	}

	@Test
	public void testParseDoubleSharpNote() throws ParseException {
		assertEquals("Note not parsed properly", 
					 new Note(NoteLetter.C, Accidental.DoubleSharp), 
					 MusicStringParser.parseNote("C##"));
	}
	
	@Test
	public void testParseFlatNote() throws ParseException {
		assertEquals("Note not parsed properly", 
					 new Note(NoteLetter.C, Accidental.Flat), 
					 MusicStringParser.parseNote("Cb"));
	}
	
	@Test
	public void testParseDoubleFlatNote() throws ParseException {
		assertEquals("Note not parsed properly", 
					 new Note(NoteLetter.C, Accidental.DoubleFlat), 
					 MusicStringParser.parseNote("Cbb"));
	}

	
	
	@Test
	public void testParseNoteLetterC() {
		assertEquals("Note Letter not parsed properly", 
				 	 NoteLetter.C, 
				 	 MusicStringParser.parseNoteLetter("C"));
	}

	@Test
	public void testParseNoteLetterD() {
		assertEquals("Note Letter not parsed properly", 
				 	 NoteLetter.D, 
				 	 MusicStringParser.parseNoteLetter("D"));
	}

	@Test
	public void testParseNoteLetterE() {
		assertEquals("Note Letter not parsed properly", 
				 	 NoteLetter.E, 
				 	 MusicStringParser.parseNoteLetter("E"));
	}

	@Test
	public void testParseNoteLetterF() {
		assertEquals("Note Letter not parsed properly", 
				 	 NoteLetter.F, 
				 	 MusicStringParser.parseNoteLetter("F"));
	}

	@Test
	public void testParseNoteLetterG() {
		assertEquals("Note Letter not parsed properly", 
				 	 NoteLetter.G, 
				 	 MusicStringParser.parseNoteLetter("G"));
	}

	@Test
	public void testParseNoteLetterA() {
		assertEquals("Note Letter not parsed properly", 
				 	 NoteLetter.A, 
				 	 MusicStringParser.parseNoteLetter("A"));
	}

	@Test
	public void testParseNoteLetterB() {
		assertEquals("Note Letter not parsed properly", 
				 	 NoteLetter.B, 
				 	 MusicStringParser.parseNoteLetter("B"));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testParseNoteLetterInvalidLetter() {
		MusicStringParser.parseNoteLetter("b");
	}
	

	@Test
	public void testParseAccidentalSharp() {
		assertEquals("Accidentals not parsed properly", 
			 	 	 Accidental.Sharp, 
			 	 	 MusicStringParser.parseAccidental("#"));
	}

	@Test
	public void testParseAccidentalDoubleSharp() {
		assertEquals("Accidentals not parsed properly", 
			 	 	 Accidental.DoubleSharp, 
			 	 	 MusicStringParser.parseAccidental("##"));
	}

	@Test
	public void testParseAccidentalFlat() {
		assertEquals("Accidentals not parsed properly", 
			 	 	 Accidental.Flat, 
			 	 	 MusicStringParser.parseAccidental("b"));
	}

	@Test
	public void testParseAccidentalDoubleFlat() {
		assertEquals("Accidentals not parsed properly", 
			 	 	 Accidental.DoubleFlat, 
			 	 	 MusicStringParser.parseAccidental("bb"));
	}
	
	@Test
	public void testParseScale() throws ArrayComparisonFailure, ParseException{
		assertArrayEquals("Scale not parsed properly", 
		 	 	 new Note[]{MusicStringParser.parseNote("C#"),
							MusicStringParser.parseNote("D##"),
							MusicStringParser.parseNote("Eb"),
							MusicStringParser.parseNote("Fbb"),
							MusicStringParser.parseNote("G"),
							MusicStringParser.parseNote("A#"),
							MusicStringParser.parseNote("B#")}, 
		 	 	 MusicStringParser.parseScale("C#D##EbFbbGA#B#"));
	}
}
