package com.basildsouza.tools.music.piano.engine;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;

import com.basildsouza.tools.music.piano.engine.chord.ChordBank;
import com.basildsouza.tools.music.piano.engine.chord.ChordDetector;
import com.basildsouza.tools.music.piano.engine.chord.PlayedChord;
import com.basildsouza.tools.music.piano.engine.event.ChordEvent;
import com.basildsouza.tools.music.piano.engine.event.ChordListener;
import com.basildsouza.tools.music.piano.engine.event.MidiKeyEvent;
import com.basildsouza.tools.music.piano.engine.event.ToneEvent;
import com.basildsouza.tools.music.piano.engine.event.ToneListener;

public class PlayerMidiKeyListenerTest {

	private PlayerMidiKeyListener playerListener;
	private ChordDetector chordDetector;

	@Before
	public void setUp() throws Exception {
		chordDetector = new ChordDetector(new ChordBank());
		playerListener = new PlayerMidiKeyListener(chordDetector);

	}

	@Test
	public void testSimpleTonePlayed() {
		ToneListener mockListener = mock(ToneListener.class);

		playerListener.addToneListener(mockListener);

		playerListener.midiKeyOn(midiKeyEvent(("C5")));
		playerListener.midiKeyOff(midiKeyEvent(("C5")));

		verify(mockListener).tonePlayed(toneEvent("C5"));
	}

	@Test
	public void testTwoTonesPlayed() {
		ToneListener mockListener = mock(ToneListener.class);

		playerListener.addToneListener(mockListener);

		playerListener.midiKeyOn(midiKeyEvent(("C5")));
		playerListener.midiKeyOn(midiKeyEvent(("D5")));
		playerListener.midiKeyOff(midiKeyEvent(("C5")));
		verify(mockListener).tonePlayed(toneEvent("C5"));

		playerListener.midiKeyOff(midiKeyEvent(("D5")));
		verify(mockListener).tonePlayed(toneEvent("D5"));
	}

	@Test
	public void testSimpleTriadChordPlayed() {
		ChordListener mockChordListener = mock(ChordListener.class);

		playerListener.addChordListener(mockChordListener);

		PlayedChord playedChord = chordDetector
				.detectChord(Arrays.asList(tone("C5"), tone("E5"), tone("G5")));
		List<Tone> playedNotes = playedChord.getPlayedNotes();
		for (Tone tone : playedNotes) {
			playerListener.midiKeyOn(midiKeyEvent(tone));
		}

		for (Tone tone : playedNotes) {
			playerListener.midiKeyOff(midiKeyEvent(tone));
		}

		verify(mockChordListener).chordPlayed(new ChordEvent(playedChord, 0));
	}

	@Test
	public void testSimpleTriadTwiceChordPlayed() {
		ChordListener mockChordListener = mock(ChordListener.class);

		playerListener.addChordListener(mockChordListener);

		PlayedChord playedChord = chordDetector
				.detectChord(Arrays.asList(tone("C5"), tone("E5"), tone("G5")));
		List<Tone> playedNotes = playedChord.getPlayedNotes();
		for (Tone tone : playedNotes) {
			playerListener.midiKeyOn(midiKeyEvent(tone));
		}

		for (Tone tone : playedNotes) {
			playerListener.midiKeyOff(midiKeyEvent(tone));
		}

		for (Tone tone : playedNotes) {
			playerListener.midiKeyOn(midiKeyEvent(tone));
		}

		for (Tone tone : playedNotes) {
			playerListener.midiKeyOff(midiKeyEvent(tone));
		}

		verify(mockChordListener, times(2)).chordPlayed(new ChordEvent(playedChord, 0));
	}

	@Test
	public void testSimpleQuartetChordPlayed() {
		ChordListener mockChordListener = mock(ChordListener.class);

		playerListener.addChordListener(mockChordListener);

		PlayedChord playedChord = chordDetector.detectChord(Arrays.asList(
				tone("C5"), tone("E5"), tone("G5"), tone("C6")));
		List<Tone> playedNotes = playedChord.getPlayedNotes();
		for (Tone tone : playedNotes) {
			playerListener.midiKeyOn(midiKeyEvent(tone));
		}

		for (Tone tone : playedNotes) {
			playerListener.midiKeyOff(midiKeyEvent(tone));
		}

		verify(mockChordListener).chordPlayed(new ChordEvent(playedChord, 0));
	}

	@Test
	public void testInterleavedQuartetChordPlayed() {
		ChordListener mockChordListener = mock(ChordListener.class);
		
		playerListener.addChordListener(mockChordListener);
		
		PlayedChord playedChord = 
				chordDetector.detectChord(Arrays.asList(tone("C5"),
														tone("E5"), 
														tone("G5"),
														tone("C5")));
		
		playerListener.midiKeyOn(midiKeyEvent(("C5")));
		playerListener.midiKeyOn(midiKeyEvent(("E5")));
		playerListener.midiKeyOn(midiKeyEvent(("G5")));
		playerListener.midiKeyOff(midiKeyEvent(("C5")));
		playerListener.midiKeyOn(midiKeyEvent(("C5")));
		playerListener.midiKeyOff(midiKeyEvent(("G5")));
		playerListener.midiKeyOff(midiKeyEvent(("E5")));
		playerListener.midiKeyOff(midiKeyEvent(("C5")));
		verify(mockChordListener).chordPlayed(new ChordEvent(playedChord, 0));
	}
	
	
	@Test
	public void testMistakenChordPlayed() {
		ChordListener mockChordListener = mock(ChordListener.class);
		ToneListener mockToneListener = mock(ToneListener.class);
		
		playerListener.addChordListener(mockChordListener);
		playerListener.addToneListener(mockToneListener);
		
		playerListener.midiKeyOn(midiKeyEvent(("C5")));
		playerListener.midiKeyOn(midiKeyEvent(("E5")));
		playerListener.midiKeyOn(midiKeyEvent(("G5")));
		playerListener.midiKeyOn(midiKeyEvent(("C6")));
		playerListener.midiKeyOn(midiKeyEvent(("E6")));
		playerListener.midiKeyOn(midiKeyEvent(("G6")));
		playerListener.midiKeyOn(midiKeyEvent(("C7")));
		playerListener.midiKeyOn(midiKeyEvent(("E7")));
		
		playerListener.midiKeyOff(midiKeyEvent(("C5")));
		verify(mockToneListener).tonePlayed(toneEvent("C5"));
		
		playerListener.midiKeyOff(midiKeyEvent(("G5")));
		verify(mockToneListener).tonePlayed(toneEvent("G5"));
		
		playerListener.midiKeyOff(midiKeyEvent(("E5")));
		verify(mockToneListener).tonePlayed(toneEvent("E5"));
		
		playerListener.midiKeyOff(midiKeyEvent(("C6")));
		verify(mockToneListener).tonePlayed(toneEvent("C6"));
		
		playerListener.midiKeyOff(midiKeyEvent(("G6")));
		verify(mockToneListener).tonePlayed(toneEvent("G6"));
		
		playerListener.midiKeyOff(midiKeyEvent(("E6")));
		verify(mockToneListener).tonePlayed(toneEvent("E6"));
		
		playerListener.midiKeyOff(midiKeyEvent(("C7")));
		verify(mockToneListener).tonePlayed(toneEvent("C7"));
		
		playerListener.midiKeyOff(midiKeyEvent(("E7")));
		verify(mockToneListener).tonePlayed(toneEvent("E7"));
		
		verify(mockChordListener, never()).chordPlayed(any(ChordEvent.class));
	}
	
	//TODO: These might go into a differnet class if they need to be shared
	private static final Pattern TONE_PATTERN = Pattern.compile("([ABCDEFG])([#b])?(\\d+)");
	
	private static ToneEvent toneEvent(String toneStr){
		return toneEvent(toneStr, 0);
	}
	
	private static ToneEvent toneEvent(String toneStr, long timestamp){
		return argThat(new ToneMatcher(tone(toneStr)));
		//return new ToneEvent(tone(toneStr), timestamp);
	}
	
	
	private static MidiKeyEvent midiKeyEvent(String toneStr){
		return midiKeyEvent(toneStr, 0);
	}
	
	private static MidiKeyEvent midiKeyEvent(Tone tone){
		return midiKeyEvent(tone, 0);
	}
	private static MidiKeyEvent midiKeyEvent(String toneStr, long timestamp){
		return midiKeyEvent(tone(toneStr), timestamp);
	}
	
	private static MidiKeyEvent midiKeyEvent(Tone tone, long timestamp){
		return new MidiKeyEvent(tone, timestamp);
	}
	
	private static Tone tone(String toneStr){
		Matcher matcher = TONE_PATTERN.matcher(toneStr);
		if(!matcher.matches() || matcher.groupCount() != 3){
			throw new IllegalArgumentException("Invalid String: " + toneStr);
		}
		
		NoteLetter noteLetter = NoteLetter.valueOf(matcher.group(1));
		int octave = Integer.parseInt(matcher.group(3));
		
		String accString = matcher.group(2);
		if("#".equals(accString)){
			return new Tone(new Note(noteLetter, Accidental.Sharp), octave); 
		} else if("b".equals(accString)){
			return new Tone(new Note(noteLetter, Accidental.Flat), octave);
		} else {
			return new Tone(new Note(noteLetter), octave);
		}
	}
	
	
	
	//TODO: Create static ToneMatcher.matches(Tone tone) [And ChordMatcher], that creates a tone matcher on the fly for the mentioned tone. 
	//To avoid matching to create tone matches everywhere
	
	private static class ToneMatcher extends ArgumentMatcher<ToneEvent> {
		private final Tone tone;
		
		public ToneMatcher(Tone tone) {
			this.tone = tone;
		}
		@Override
		public boolean matches(Object argument) {
			if(!(argument instanceof ToneEvent)){
				return false;
			}
			return tone.equals(((ToneEvent)argument).getPlayedTone());
		}
		
	}
	
	private static class ChordMatcher extends ArgumentMatcher<ChordEvent> {
		private final PlayedChord playedChord;
		
		public ChordMatcher(PlayedChord playedChord) {
			this.playedChord = playedChord;
		}
		@Override
		public boolean matches(Object argument) {
			if(!(argument instanceof ChordEvent)){
				return false;
			}
			return playedChord.equals(((ChordEvent)argument).getPlayedChord());
		}
		
	}
	
}
