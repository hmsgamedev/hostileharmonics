package com.basildsouza.tools.music.piano.recorder;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.basildsouza.tools.music.piano.engine.io.Scale;
import javax.swing.UIManager;

public class ScaleRecorder extends JDialog {
	private Scale playedScale;

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ScaleRecorder dialog = new ScaleRecorder(null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ScaleRecorder(JDialog owner) {
		super(owner, "Scale Selector", true);
		setBounds(100, 100, 522, 362);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JPanel pnlScaleButton = new JPanel();
		pnlScaleButton.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Click on a scale to select", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlScaleButton.setBounds(10, 11, 486, 269);
		contentPanel.add(pnlScaleButton);
		pnlScaleButton.setLayout(new GridLayout(0, 3, 7, 0));
		
		for(final Scale scale : Scale.values()){
			JButton btnScale = new JButton("<html><center>" + scale.name() + "</center>" + scale.getOctaveSample());
			btnScale.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					playedScale = scale;
					System.out.println("Inrecorder: " + playedScale);
					ScaleRecorder.this.dispose();
				}
			});
			
			pnlScaleButton.add(btnScale);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						playedScale = null;
						ScaleRecorder.this.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				getRootPane().setDefaultButton(cancelButton);
			}
		}
	}

	public Scale getPlayedScale() {
		return playedScale;
	}
}
