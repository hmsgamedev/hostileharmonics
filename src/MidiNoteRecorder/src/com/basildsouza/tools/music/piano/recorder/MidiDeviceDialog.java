package com.basildsouza.tools.music.piano.recorder;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiUnavailableException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.basildsouza.tools.music.piano.engine.MidiEngine;

public class MidiDeviceDialog extends JDialog {
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private MidiEngine selectedDevice;
	private JComboBox cmbDeviceList;
	private List<MidiDevice> devices;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MidiDeviceDialog dialog = new MidiDeviceDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private MidiDeviceDialog() {
		this(null, Collections.<MidiDevice>emptyList());
	}

	/**
	 * Create the dialog.
	 * @param list 
	 */
	public MidiDeviceDialog(Dialog owner, List<MidiDevice> midiDevices) {
		super(owner, "Select Midi Device", true);
		setBounds(100, 100, 325, 141);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblMidiDevices = new JLabel("Midi Devices");
			lblMidiDevices.setBounds(10, 11, 58, 14);
			contentPanel.add(lblMidiDevices);
		}
		
		cmbDeviceList = new JComboBox();
		cmbDeviceList.setBounds(10, 35, 289, 20);
		contentPanel.add(cmbDeviceList);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							selectedDevice = new MidiEngine(devices.get(cmbDeviceList.getSelectedIndex()));
						} catch (MidiUnavailableException e1) {
							JOptionPane.showMessageDialog
										(MidiDeviceDialog.this, 
										 "Unable to load midi device, please try again", 
										 "Error selecting device", JOptionPane.ERROR_MESSAGE);
							e1.printStackTrace();
							return;
						}
						
						MidiDeviceDialog.this.dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						selectedDevice = null;
						MidiDeviceDialog.this.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			
			populateMidiDeviceList(midiDevices);
		}
	}
	
	public MidiEngine getSelectedDevice(){
		return selectedDevice;
	}
	
	public void populateMidiDeviceList(List<MidiDevice> devices){
		this.devices = devices;
		for(MidiDevice device : devices){
			cmbDeviceList.addItem(device.getDeviceInfo().getName());
		}
	}
}
