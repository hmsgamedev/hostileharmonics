package com.basildsouza.tools.music.piano.recorder;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;

import com.basildsouza.tools.music.piano.engine.MidiDeviceLister;
import com.basildsouza.tools.music.piano.engine.MidiEngine;
import com.basildsouza.tools.music.piano.engine.Note;
import com.basildsouza.tools.music.piano.engine.NoteLetter;
import com.basildsouza.tools.music.piano.engine.PlayerMidiKeyListener;
import com.basildsouza.tools.music.piano.engine.Tone;
import com.basildsouza.tools.music.piano.engine.chord.Chord;
import com.basildsouza.tools.music.piano.engine.chord.ChordBank;
import com.basildsouza.tools.music.piano.engine.chord.ChordDetector;
import com.basildsouza.tools.music.piano.engine.event.ChordEvent;
import com.basildsouza.tools.music.piano.engine.event.ChordListener;
import com.basildsouza.tools.music.piano.engine.event.ToneEvent;
import com.basildsouza.tools.music.piano.engine.event.ToneListener;
import com.basildsouza.tools.music.piano.engine.io.Familiarity;
import com.basildsouza.tools.music.piano.engine.io.Scale;
import com.basildsouza.tools.music.piano.engine.io.Tune;
import com.basildsouza.tools.music.piano.engine.io.Tune.Phrase;
import com.basildsouza.tools.music.piano.engine.io.TuneIO;

public class PianoRecorder extends JDialog {
	private static final String PHRASE_SEP = "\n";
	private static final String TONE_SEP = ", ";
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtFileName;
	private JTextField txtOriginalScale;
	private JTextField txtSongName;
	
	private JButton btnChangeFile;
	private JButton btnChangeSong;

	
	private static final int NOTE_RECORDING = 0;
	private static final int SCALE_RECORDING = 1;
	private int recordingMode;
	
	private File chosenFile;
	private String songName;
	private MidiEngine midiEngine;
	private List<Tone> playedTones = new ArrayList<Tone>(200);
	
	private Scale originalScale;
	private PlayerMidiKeyListener playerMidiKeyListener;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			PianoRecorder dialog = new PianoRecorder();
			
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PianoRecorder() {
		setBounds(100, 100, 600, 500);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Metadata", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 564, 100);
		contentPanel.add(panel);
		panel.setLayout(null);
		
		JLabel lblFileName = new JLabel("File Name:");
		lblFileName.setBounds(10, 21, 75, 14);
		panel.add(lblFileName);
		
		JLabel lblSongName = new JLabel("Song Name:");
		lblSongName.setBounds(10, 46, 75, 14);
		panel.add(lblSongName);
		
		JLabel lblOriginalScale = new JLabel("Original Scale:");
		lblOriginalScale.setBounds(10, 71, 75, 14);
		panel.add(lblOriginalScale);
		
		txtFileName = new JTextField();
		txtFileName.setEditable(false);
		txtFileName.setBounds(95, 18, 342, 20);
		panel.add(txtFileName);
		txtFileName.setColumns(10);
		
		txtOriginalScale = new JTextField();
		txtOriginalScale.setEditable(false);
		txtOriginalScale.setBounds(95, 68, 342, 20);
		panel.add(txtOriginalScale);
		txtOriginalScale.setColumns(10);
		
		txtSongName = new JTextField();
		txtSongName.setEditable(false);
		txtSongName.setBounds(95, 43, 342, 20);
		panel.add(txtSongName);
		txtSongName.setColumns(10);
		
		btnChangeFile = new JButton("Change (F Maj)");
		btnChangeFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fileSelectAction();
			}
		});
		btnChangeFile.setBounds(447, 17, 107, 23);
		panel.add(btnChangeFile);
		
		btnChangeSong = new JButton("Change (A Min)");
		btnChangeSong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				songNameAction();
			}
		});
		btnChangeSong.setBounds(447, 42, 107, 23);
		panel.add(btnChangeSong);
		
		JButton btnChangeScale = new JButton("Change (C Maj)");
		btnChangeScale.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				originalScaleAction();
			}
		});
		btnChangeScale.setBounds(447, 67, 107, 23);
		panel.add(btnChangeScale);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Song", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 122, 564, 296);
		contentPanel.add(panel_1);
		panel_1.setLayout(null);
		
		txtPnSong = new JTextPane();
		txtPnSong.setBounds(10, 24, 544, 226);
		panel_1.add(txtPnSong);
		
		JLabel lblLowestKeyTo = new JLabel(" Lowest Key to backspace");
		lblLowestKeyTo.setBounds(10, 271, 132, 14);
		panel_1.add(lblLowestKeyTo);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			
			JButton btnMidiSettings = new JButton("Midi Settings");
			btnMidiSettings.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					MidiEngine tempMidiEngine = selectMidiDevice();
					if(tempMidiEngine == null){
						return;
					}
					if(midiEngine != null){
						midiEngine.close();
					}
					midiEngine = tempMidiEngine;
				}
			});
			buttonPane.add(btnMidiSettings);
			{
				JButton saveButton = new JButton("Save");
				saveButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						save();
					}
				});
				saveButton.setActionCommand("OK");
				buttonPane.add(saveButton);
				getRootPane().setDefaultButton(saveButton);
			}
			{
				JButton saveAndNewButton = new JButton("Save And New");
				saveAndNewButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						save();
					}
				});
				buttonPane.add(saveAndNewButton);
			}
		}
	}
	
	public void save(){
		if(originalScale == null){
			//TODO: Add error message JOptionPane
			return;
		}
		
		if(chosenFile == null){
			//TODO: Add error message JOptionPane
			return ;
		}
		
		if(songName == null || songName.isEmpty()){
			//TODO: Add error message JOptionPane
			return;
		}
		
		TuneIO tuneWriter = new TuneIO();
		
		Tune tune = new Tune(songName, Familiarity.UNKNOWN, originalScale, convertToPhrases(playedTones));
		try {
			tuneWriter.store(tune, chosenFile.getAbsolutePath());
		} catch (IOException e) {
			//TODO: Add error message JOptionPane
			System.out.println("Unable to write");
			e.printStackTrace();
		}
		
		//TODO: To remove the only save button and reset everything. Or do i support overwrite?
	}
	
	private List<Phrase> convertToPhrases(List<Tone> nullSepToneList){
		final List<Phrase> phraseList = new ArrayList<Phrase>(20);
		List<Tone> tempTone = new ArrayList<Tone>(30);
		for(Tone currTone : nullSepToneList){
			if(currTone == null){
				Tone[][] tempArr = new Tone[2][tempTone.size()];
				//tempArr[Phrase.BASS_PHRASE] = new Tone[];
				tempTone.toArray(tempArr[Phrase.TREBLE_PHRASE]);
				tempTone.clear();
				
				phraseList.add(new Phrase(tempArr));
				continue;
			}
			
			tempTone.add(currTone);
		}
		
		return phraseList;
	}
	
	public File selectFile(){
		JFileChooser fileChooser = new JFileChooser(new File("./data"));
		fileChooser.setFileFilter(new FileFilter() {
			
			@Override
			public String getDescription() {
				return "Text Files";
			}
			
			@Override
			public boolean accept(File f) {
				return f!= null && !f.exists() && f.getName().endsWith(".txt");
			}
		});
		int userAction = fileChooser.showSaveDialog(PianoRecorder.this);
		if(JFileChooser.CANCEL_OPTION == userAction){
			return null;
		}
		if(JFileChooser.ERROR_OPTION == userAction){
			return null;
		}

		return fileChooser.getSelectedFile();
	}
	
	public String selectSongName(){
		return JOptionPane.showInputDialog(this, "Enter Song Name");
	}
	
	public Scale selectScale(){
		originalScaleRecStart();
		
		ScaleRecorder scaleRecorder = new ScaleRecorder(this);
		scaleRecorder.setVisible(true);
		Scale playedScale = scaleRecorder.getPlayedScale();
		System.out.println(playedScale);
		originalScaleRecStop();
		return playedScale;
	}
	
	public MidiEngine selectMidiDevice(){
		MidiDeviceLister lister = MidiDeviceLister.getInstance();
		lister.refresh();
		MidiDeviceDialog midiDevices = new MidiDeviceDialog(this, lister.getMidiInputDevices());
		midiDevices.setVisible(true);
		
		if(midiDevices.getSelectedDevice() == null){
			return null;
		}
		ChordBank chordBank = new ChordBank();
		ChordDetector chordDetector = new ChordDetector(chordBank);
		
		playerMidiKeyListener = new PlayerMidiKeyListener(chordDetector);
		playerMidiKeyListener.addChordListener(chordListener);
		playerMidiKeyListener.addToneListener(songRecListener);

		MidiEngine selectedDevice = midiDevices.getSelectedDevice();
		selectedDevice.addMidiKeyListener(playerMidiKeyListener);
		return selectedDevice;
	}
	
	
	private ChordListener chordListener = new ChordListener() {
		
		@Override
		public void chordPlayed(ChordEvent ce) {
			Chord chord = ce.getPlayedChord().getChord();
			System.out.println("Chord: " + chord);
			if(chord.equals(Chord.UNKNOWN_CHORD)){
				return;
			}
			
			if(chord.equals(FILE_CHOOSE)){
				fileSelectAction();
				System.out.println("File Select");
				return;
			}
			if(chord.equals(SONG_NAME)){
				songNameAction();
				System.out.println("Song Name");
				return;
			}
			if(chord.equals(SCALE_CHANGE)){
				originalScaleAction();
				System.out.println("Scale Change");
				return;
			}
			
			if(chord.equals(PHRASE_END)){
				addPhraseSeparator();
				System.out.println("Phrase End");
				return;
			}
			
		}
	};

	private static final Chord FILE_CHOOSE = ChordBank.F_MAJ;
	private static final Chord SONG_NAME = ChordBank.A_MIN;
	private static final Chord SCALE_CHANGE = ChordBank.C_MAJ;
	private static final Chord PHRASE_END = ChordBank.C_MIN;
	
	
	
	private static final Tone lowestNote = new Tone(new Note(NoteLetter.G), 3);
	private ToneListener scaleRecListener = new ToneListener() {
		
		@Override
		public void tonePlayed(ToneEvent te) {
			// This isnt really required anymore actually
		}
	};
	
	private void addPhraseSeparator(){
		playedTones.add(null);
		addToneToDisplay(null);
	}
	
	private ToneListener songRecListener = new ToneListener() {
		
		@Override
		public void tonePlayed(ToneEvent te) {
			final Tone tone = new Tone(originalScale.getScaledNote(te.getPlayedTone().getNote()), 
									   te.getPlayedTone().getOctave());
			
			if(tone.getTonicDistance() < lowestNote.getTonicDistance()){
				if(playedTones.isEmpty()){
					return;
				}
				final Tone oldTone  = playedTones.remove(playedTones.size()-1);
				removeToneFromDisplay(oldTone);
				return;
			}
			
			playedTones.add(tone);
			addToneToDisplay(tone);
		}
	};
	
	public void addToneToDisplay(Tone tone){
		final String toneStr;
		if(tone == null){
			toneStr = PHRASE_SEP;
		} else {
			toneStr = tone.toString() + TONE_SEP;
		}
		
		txtPnSong.setText(txtPnSong.getText() + toneStr);
	}
	
	public void removeToneFromDisplay(Tone tone){
		final int toneLen;
		if(tone == null){
			toneLen = PHRASE_SEP.length(); 
		} else {
			toneLen = tone.toString().length() + TONE_SEP.length();
		}
		String currTxt = txtPnSong.getText();
		txtPnSong.setText(currTxt.substring(0, currTxt.length() - toneLen));
	}
	
	private JTextPane txtPnSong;
	
	private void originalScaleAction() {
		Scale tempScale = selectScale();
		if(tempScale == null){
			return;
		}
		
		originalScale = tempScale;
		txtOriginalScale.setText(originalScale.getOctaveSample());
	}
	
	private void songNameAction() {
		String tempSong = selectSongName();
		if(tempSong == null){
			return;
		}
		
		songName = tempSong;
		txtSongName.setText(songName);
	}

	private void fileSelectAction() {
		File tempFile = selectFile();
		if(tempFile == null){
			return;
		}
		chosenFile = tempFile; 
		
		txtFileName.setText(chosenFile.getAbsolutePath());
	}
	
	private void originalScaleRecStart(){
		if(playerMidiKeyListener == null){
			return;
		}
		playerMidiKeyListener.removeToneListener(songRecListener);
		playerMidiKeyListener.addToneListener(scaleRecListener);
	}
	
	private void originalScaleRecStop(){
		if(playerMidiKeyListener == null){
			return;
		}
		playerMidiKeyListener.removeToneListener(scaleRecListener);
		playerMidiKeyListener.addToneListener(songRecListener);
		
	}
}
